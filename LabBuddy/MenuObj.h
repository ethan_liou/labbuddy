//
//  MenuObj.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 12/5/28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MenuObj : NSObject{
	NSString * title_;
	NSString * img_name_;
	UIViewController * ctl_;
}

+(id)MenuWithTitle:(NSString*)title Img:(NSString*)img_name Controller:(UIViewController*)ctl;
-(id)initWithTitle:(NSString*)title Img:(NSString*)img_name Controller:(UIViewController*)ctl;
-(NSString*)title;
-(NSString*)img_name;
-(UIViewController*)ctl;

@end
