//
//  DailyHistoryViewController.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/12/24.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DailyHistoryViewController.h"
#import "DBHelper.h"

@implementation DailyHistoryViewController

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

- (id)initWithDate:(NSString *)date{
	self = [super init];
    if (self) {
        // Custom initialization.
		dateString = [[NSString alloc] initWithString:date];		
		DBHelper * helper = [DBHelper newInstance];
		entrys = [[NSMutableArray alloc] initWithArray:[helper queryHistory:date]];
    }
    return self;
	
}

-(void)toggleEditing{
	UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
	button.bounds = CGRectMake(0, 0, 65.0, 30.0);
	editing = !editing;
	[controller setEdit:editing];

	NSString * imageName = editing ? @"done_btn.png" : @"edit_btn.png";
	NSString * title = editing ? NSLocalizedString(@"Done", @"Done") : NSLocalizedString(@"Edit", @"Edit");
	[button setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
	[button setTitle:title forState:UIControlStateNormal];
	button.titleLabel.font = [UIFont boldSystemFontOfSize:14];
	[button addTarget:self action:@selector(toggleEditing) forControlEvents:UIControlEventTouchUpInside];
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
	button.bounds = CGRectMake(0, 0, 65.0, 30.0);
    [button setBackgroundImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
	[button setTitle:NSLocalizedString(@"Back", @"Back") forState:UIControlStateNormal];
   	button.titleLabel.font = [UIFont boldSystemFontOfSize:14];
	button.titleEdgeInsets = UIEdgeInsetsMake(0, 12, 0, 0);
	[button addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
	dateTF.text = [NSString stringWithFormat:@"    %@",dateString];
	[controller setParent:self];
	[controller setEntrys:entrys];
	[controller setDateString:dateString];
	editing = NO;
	button = [UIButton buttonWithType:UIButtonTypeCustom];
	button.bounds = CGRectMake(0, 0, 65.0, 30.0);
	NSString * imageName = editing ? @"done_btn.png" : @"edit_btn.png";
	NSString * title = editing ? NSLocalizedString(@"Done", @"Done") : NSLocalizedString(@"Edit", @"Edit");
	[button setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
	[button setTitle:title forState:UIControlStateNormal];
	button.titleLabel.font = [UIFont boldSystemFontOfSize:14];
	[button addTarget:self action:@selector(toggleEditing) forControlEvents:UIControlEventTouchUpInside];
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
}

- (void)showTab:(BOOL)isShow{
  if(isShow){
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    for(UIView *view in self.tabBarController.view.subviews)
    {
      if([view isKindOfClass:[UITabBar class]]){
        [view setFrame:CGRectMake(view.frame.origin.x, 480-49, view.frame.size.width, view.frame.size.height)];
      } 
      else {
        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 480-49)];
      }
    }
    [UIView commitAnimations];	
  }else{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    for(UIView *view in self.tabBarController.view.subviews){
      if([view isKindOfClass:[UITabBar class]])
      {
        [view setFrame:CGRectMake(view.frame.origin.x, 480, view.frame.size.width, view.frame.size.height)];
      } 
      else 
      {
        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 480)];
      }
      
    }	
    [UIView commitAnimations];
  }
}

- (void)viewWillAppear:(BOOL)animated{
  [self showTab:YES];
}	

- (void)backAction{
	[self.navigationController popViewControllerAnimated:YES];
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


@end
