//
//  CalendarObj.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 12/7/7.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalendarObj : NSObject

@property (nonatomic) NSInteger year_;
@property (nonatomic) NSInteger month_;
@property (nonatomic,strong) NSMutableArray * day_obj_arr_;

@property (nonatomic, strong) NSDictionary * basic_history_dict_;
@property (nonatomic, strong) NSDictionary * history_dict_;

-(void)generateMonth;

@end
