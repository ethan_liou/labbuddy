//
//  DataTableViewController.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/10/14.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ItemCell.h"

@interface DataTableViewController : UITableViewController {
	IBOutlet ItemCell * cell;
	BOOL isEdit;
}

-(void)setEdit:(BOOL)edit;
-(IBAction) deleteCell:(id)sender;

@end
