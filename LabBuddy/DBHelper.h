//
//  DBHelper.h
//  HomeSeller
//
//  Created by Liou Yu-Cheng on 2011/10/1.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <sqlite3.h>
#import "Entry.h"
#import "TypeDefininition.h"

@interface DBHelper : NSObject {
	sqlite3 * database;
}

@property(readonly, nonatomic) sqlite3 *database;

- (BOOL) addBasicHistory:(NSMutableArray*)basic_data;
+ (DBHelper *) newInstance;
- (void) openDatabase;
- (void) closeDatabase;
- (NSString *) getDatabaseFullPath;
- (void) copyDatabaseIfNeeded;
- (sqlite3_stmt *) executeQuery:(NSString *) query;
- (Entry *) makeEntry:(sqlite3_stmt *)statement;
- (BOOL) addHistory:(NSMutableArray*)indexes;
- (NSMutableArray*) queryHistory:(NSString*)dateString;
- (BOOL) removeDateHistory:(NSString*)date;
- (BOOL) removeOneHistory:(NSString*)date index:(NSInteger)index category:(NSInteger)category;
- (NSMutableArray*) queryDate;
- (NSMutableArray*) queryBookmark;
- (BOOL) removeBookmarkWithCategory:(NSInteger)category Index:(NSInteger)index;
- (BOOL) addBookmarkWithCategory:(NSInteger)category Index:(NSInteger)index;
- (NSMutableDictionary*) queryHistoryByYear:(NSInteger)year AndMonth:(NSInteger)month;
- (NSMutableDictionary*) queryBasicHistoryByYear:(NSInteger)year AndMonth:(NSInteger)month;
- (NSArray *) queryBasicByType:(enum BasicHistoryType)type;

@end
