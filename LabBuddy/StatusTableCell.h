//
//  StatusTableCell.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/12/24.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Entry.h"

@interface StatusTableCell : UITableViewCell {
	IBOutlet UIImageView * headerIV;
	IBOutlet UILabel * headerL;
	IBOutlet UILabel * contentL;
	IBOutlet UITextField * suggestTF;
}

- (void)loadEntry:(Entry*)e;

@end
