//
//  SearchTerm.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/12/19.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SearchTerm.h"
#import "Utils.h"

@implementation SearchTerm

@synthesize formal_name,stringToShow,entryIndexes;

-(id)initWithArray:(NSArray*)array{
	self = [super init];
    if (self) {
		formal_name = [array objectAtIndex:0];
		chinese_name = [array objectAtIndex:1];
		eng_sym = [array objectAtIndex:2];
		cht_sym = [array objectAtIndex:3];
		stringToShow = nil;
		entryIndexes = [[NSMutableArray alloc] init];
	}
	return self;
}

-(void)addIndex:(NSInteger)index{
	[entryIndexes addObject:[NSNumber numberWithInt:index]];
}

-(BOOL)match:(NSString*)prefix{
	NSString * eng = nil;
	NSString * cht = nil;
	
	if ([Utils is:formal_name startWith:prefix]) {
		eng = formal_name;
	}
	else{
		NSArray *eng_symArray = [eng_sym componentsSeparatedByString: @"、"];
		for(NSString * str in eng_symArray){
			if([Utils is:str startWith:prefix]){
				eng = str;
				break;
			}
		}
	}
	
	if ([Utils is:chinese_name startWith:prefix]) {
		cht = chinese_name;
	}
	else{
		NSArray *cht_symArray = [cht_sym componentsSeparatedByString: @"、"];
		for(NSString * str in cht_symArray){
			if([Utils is:str startWith:prefix]){
				cht = str;
				break;
			}
		}
	}
	
	if (eng == nil && cht == nil) {
		return NO;
	}
	if (eng == nil) {
		eng = formal_name;
	}
	if (cht == nil) {
		cht = chinese_name;
	}
	eng = [eng stringByReplacingOccurrencesOfString:@"\n" withString:@""];
	cht = [cht stringByReplacingOccurrencesOfString:@"\n" withString:@""];	
	stringToShow = [[NSString alloc] initWithFormat:@"%@、%@",eng,cht];
	return YES;
}

@end
