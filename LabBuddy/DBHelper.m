//
//  DBHelper.m
//  HomeSeller
//
//  Created by Liou Yu-Cheng on 2011/10/1.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DBHelper.h"
#import "AppDelegate.h"
#import "Utils.h"
#import "BasicHistory.h"

@implementation DBHelper

static DBHelper *instance = nil;

NSString *DB_NAME = @"labbuddy";
NSString *DB_EXT = @".sqlite";

@synthesize database;

- (NSDate*)getDate{
	AppDelegate * delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	if (delegate.targetDate == nil) {
		return [NSDate date];
	}
	return delegate.targetDate;
}

+ (DBHelper *) newInstance{
	@synchronized(self) {
		if (instance == nil){
			instance = [[DBHelper alloc]init];
			[instance openDatabase];
		}
	}
	return instance;
}

+ (id)allocWithZone:(NSZone *)zone {
	@synchronized(self) {
		if (instance == nil) {
			instance = [super allocWithZone:zone];
			return instance;        
		}
	}
	return nil;
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

- (void) openDatabase{
    if (!database){
		[self copyDatabaseIfNeeded];
		int result = sqlite3_open([[self getDatabaseFullPath] UTF8String], &database);
		printf("open %d %d\n",result,SQLITE_OK);
		if (result != SQLITE_OK){
			NSAssert(0, @"Failed to open database");
		}
	}
}

- (void) closeDatabase{
    if (database){
        sqlite3_close(database);
    }
}

- (void) copyDatabaseIfNeeded{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSString *dbPath = [self getDatabaseFullPath];
    BOOL success = [fileManager fileExistsAtPath:dbPath]; 
	
    if(!success) {
		
        NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@", DB_NAME, DB_EXT]];
        success = [fileManager copyItemAtPath:defaultDBPath toPath:dbPath error:&error];
        NSLog(@"Database file copied from bundle to %@", dbPath);
		
        if (!success){ 
            NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
        }
		
    } else {
		
        NSLog(@"Database file found at path %@", dbPath);
		
    }
}

- (NSString *) getDatabaseFullPath{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@", DB_NAME, DB_EXT]];
	return path;
}

- (sqlite3_stmt *) executeQuery:(NSString *) query{
	sqlite3_stmt *statement;
	sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil);
	return statement;
}

-(BOOL)execSQL:(NSString *)sql{
	char * err_msg = NULL;
	NSLog(@"execSQL : %@",sql);
	int rc = sqlite3_exec(database, [sql UTF8String], NULL, NULL, &err_msg);
	if(rc != SQLITE_OK){
		NSLog(@"sqlite err(%d) : %s",rc,err_msg);	
		return NO;
	}	
	return YES;
}

- (Entry *) makeEntry:(sqlite3_stmt *)statement{
	NSMutableArray * array = [[NSMutableArray alloc] init];
	int column_cnt = sqlite3_column_count(statement);
	for(int i = 0 ; i <= column_cnt ; i ++){
		char * tempStr = (char *)sqlite3_column_text(statement, i);
		if(tempStr == NULL){
			[array addObject:@""];
		}
		else {
			[array addObject:[NSString stringWithUTF8String:tempStr]];
		}
	}
	return [[Entry alloc] initWithArray:array];
}

- (NSMutableArray*) queryDate{
	NSMutableArray * retArray = [[NSMutableArray alloc] init];
	sqlite3_stmt *statement;
	sqlite3_prepare_v2(database, "SELECT DISTINCT date FROM history", -1, &statement, nil);
	while(sqlite3_step(statement) == SQLITE_ROW){
		char * date = (char*)sqlite3_column_text(statement, 0);
		NSString * str = [[NSString alloc] initWithUTF8String:date];
		[retArray addObject:str];
	}
	return retArray;
}

- (NSMutableDictionary*) queryHistoryByYear:(NSInteger)year AndMonth:(NSInteger)month{
	NSInteger next_year = month == 12 ? year + 1 : year;
	NSInteger next_month = month == 12 ? 1 : month + 1;
	NSMutableDictionary * retDict = [[NSMutableDictionary alloc] initWithCapacity:31];
	NSString * queryString = [NSString stringWithFormat:@"SELECT * FROM history WHERE date >= '%d/%d/01' and date < '%d/%d/01'",year,month,next_year,next_month];
	sqlite3_stmt *statement;
	sqlite3_prepare_v2(database, [queryString UTF8String], -1, &statement, nil);
	AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
	while(sqlite3_step(statement) == SQLITE_ROW){
		NSString * dateStr = [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 0)];
		NSInteger category = sqlite3_column_int(statement, 1);
		NSInteger index = sqlite3_column_int(statement, 2);
		NSString * value = [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 3)];
		NSMutableArray * entrys = [delegate.everyEntry objectAtIndex:category];
		Entry * entry = (Entry*)[entrys objectAtIndex:index];
		entry.value = value;
		entry.category = category;
		entry.index = index;
		if ([retDict objectForKey:dateStr] == nil) {
			// insert a empty array
			NSMutableArray * arr = [[NSMutableArray alloc] init];
			[retDict setObject:arr forKey:dateStr];
		}
		NSMutableArray * arr = [retDict objectForKey:dateStr];
		[arr addObject:entry];
	}
	return retDict;
}

- (NSMutableDictionary*) queryBasicHistoryByYear:(NSInteger)year AndMonth:(NSInteger)month{
	NSInteger next_year = month == 12 ? year + 1 : year;
	NSInteger next_month = month == 12 ? 1 : month + 1;
	NSMutableDictionary * retDict = [[NSMutableDictionary alloc] initWithCapacity:31];
	NSString * queryString = [NSString stringWithFormat:@"SELECT * FROM basic_history WHERE _date >= \"%04d/%02d/01\" and _date < \"%04d/%02d/01\"",year,month,next_year,next_month];
	NSLog(@"%@",queryString);
	sqlite3_stmt *statement;
	sqlite3_prepare_v2(database, [queryString UTF8String], -1, &statement, nil);
	while(sqlite3_step(statement) == SQLITE_ROW){
		NSString * dateStr = [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
		BasicHistory * bh = [[BasicHistory alloc] initWithSqliteStatement:statement];
		if ([retDict objectForKey:dateStr] == nil) {
			// insert a empty array
			NSMutableArray * arr = [[NSMutableArray alloc] init];
			[retDict setObject:arr forKey:dateStr];
		}
		NSMutableArray * arr = [retDict objectForKey:dateStr];
		[arr addObject:bh];
	}
	return retDict;
}

- (NSMutableArray*) queryHistory:(NSString*)dateString{
	NSMutableArray * retArray = [[NSMutableArray alloc] init] ;
	NSString * queryString = [NSString stringWithFormat:@"SELECT * FROM history WHERE date='%@'",dateString];
	NSLog(@"%@",queryString);
	sqlite3_stmt *statement;
	sqlite3_prepare_v2(database, [queryString UTF8String], -1, &statement, nil);
	AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
	while(sqlite3_step(statement) == SQLITE_ROW){
		NSInteger category = sqlite3_column_int(statement, 1);
		NSInteger index = sqlite3_column_int(statement, 2);
		NSString * value = [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 3)];
		NSMutableArray * entrys = [delegate.everyEntry objectAtIndex:category];
		Entry * entry = (Entry*)[entrys objectAtIndex:index];
		entry.value = value;
    entry.category = category;
    entry.index = index;
		NSLog(@"%d %d %@",category,index,entry.value);
		[retArray addObject:entry];
	}
	return retArray;
}

- (BOOL) removeOneHistory:(NSString*)date index:(NSInteger)index category:(NSInteger)category{
  NSString * rmString = [NSString stringWithFormat:@"DELETE FROM history WHERE \"date\"='%@' AND \"index\"=%d AND \"category\" = %d",date,index,category];
  NSLog(@"%@",rmString);
  sqlite3_stmt *statement;
  sqlite3_prepare_v2(database, [rmString UTF8String], -1, &statement, nil);
  if (sqlite3_step(statement) == SQLITE_DONE)
  {
    NSLog(@"remove successful");
  } else {
    NSLog(@"remove fail");
  }
	return YES;
}

- (BOOL) removeDateHistory:(NSString*)date{
	//	get date  
  NSString * rmString = [NSString stringWithFormat:@"DELETE FROM history WHERE date = '%@'",date];
  sqlite3_stmt *statement;
  sqlite3_prepare_v2(database, [rmString UTF8String], -1, &statement, nil);
  if (sqlite3_step(statement) == SQLITE_DONE)
  {
    NSLog(@"remove successful");
  } else {
    NSLog(@"remove fail");
  }
	return YES;
}

- (BOOL) addBasicHistory:(NSMutableArray*)basic_data{
	NSInteger user_id = [Utils getUserId];
	NSDate *date = [self getDate];
	NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
	[dateFormat setDateFormat:@"YYYY/MM/dd"];
	NSString *dateString = [dateFormat stringFromDate:date];
	NSMutableString * values = [[NSMutableString alloc] init];
	for(NSString * str in basic_data){
		[values appendFormat:@",%@",str];
	}
	NSString * sql = [NSString stringWithFormat:@"INSERT INTO basic_history VALUES (%d,'%@'%@)",
					  user_id,dateString,values];
	NSLog(@"%@",sql);
	return [self execSQL:sql];
}

- (BOOL) addHistory:(NSMutableArray*)indexes{
	//	get date
	NSDate *date = [self getDate];
	NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
	[dateFormat setDateFormat:@"YYYY/MM/dd"];
	NSString *dateString = [dateFormat stringFromDate:date];  
	for (NSArray * array in indexes) {
		//	category,index,value
		NSInteger category = [(NSNumber*)[array objectAtIndex:0] intValue];
		NSInteger index = [(NSNumber*)[array objectAtIndex:1] intValue];
		NSString * value = (NSString*)[array objectAtIndex:2];
		NSString * addString = [NSString stringWithFormat:@"INSERT INTO history VALUES('%@',%d,%d,'%@')",dateString,category,index,value];
		NSLog(@"%@",addString);
		sqlite3_stmt *statement;
		sqlite3_prepare_v2(database, [addString UTF8String], -1, &statement, nil);
		if (sqlite3_step(statement) == SQLITE_DONE)
		{
			NSLog(@"add successful");
		} else {
			NSLog(@"add fail");
		}
		
	}
	return YES;
}

- (NSMutableArray*) queryBookmark{
	NSMutableArray * retArray = [[NSMutableArray alloc] init];
	NSString * queryString = @"SELECT * FROM bookmark";
	sqlite3_stmt *statement;
	sqlite3_prepare_v2(database, [queryString UTF8String], -1, &statement, nil);
	AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
	while(sqlite3_step(statement) == SQLITE_ROW){
		NSInteger category = sqlite3_column_int(statement, 0);
		NSInteger index = sqlite3_column_int(statement, 1);
		NSMutableArray * entrys = [delegate.everyEntry objectAtIndex:category];
		Entry * entry = (Entry*)[entrys objectAtIndex:index];
    entry.category = category;
    entry.index = index;
		[retArray addObject:entry];
	}
	return retArray;	
}

- (BOOL) removeBookmarkWithCategory:(NSInteger)category Index:(NSInteger)index{
  NSString * rmString = [NSString stringWithFormat:@"DELETE FROM bookmark WHERE \"index\"=%d AND \"category\" = %d",index,category];
  sqlite3_stmt *statement;
  sqlite3_prepare_v2(database, [rmString UTF8String], -1, &statement, nil);
  if (sqlite3_step(statement) == SQLITE_DONE)
  {
    NSLog(@"remove successful");
  } else {
    NSLog(@"remove fail");
  }
	return YES;
}

- (BOOL) addBookmarkWithCategory:(NSInteger)category Index:(NSInteger)index{
	NSString * addString = [NSString stringWithFormat:@"INSERT INTO bookmark VALUES(%d,%d)",category,index];
	NSLog(@"%@",addString);
	sqlite3_stmt *statement;
	sqlite3_prepare_v2(database, [addString UTF8String], -1, &statement, nil);
	if (sqlite3_step(statement) == SQLITE_DONE)
	{
		NSLog(@"add successful");
	} else {
		NSLog(@"add fail");
	}
	return YES;
}

- (NSArray *) queryBasicByType:(enum BasicHistoryType)type{
	// return date, val array
	NSMutableArray * retArray = [[NSMutableArray alloc] init];
	NSString * querySql = [NSString stringWithFormat:@"SELECT _date,%@ FROM basic_history WHERE %@ is not null order by _date desc",[BasicHistory typeToDBColumnName:type],[BasicHistory typeToDBColumnName:type]];
	NSLog(@"%@",querySql);
	sqlite3_stmt *statement;
	sqlite3_prepare_v2(database, [querySql UTF8String], -1, &statement, nil);
	while(sqlite3_step(statement) == SQLITE_ROW){
		NSString * dateStr = [NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 0)];
		CGFloat val = sqlite3_column_double(statement, 1);
		[retArray addObject:[NSArray arrayWithObjects:dateStr,[NSNumber numberWithFloat:val], nil]];
	}
	return retArray;

}

@end
