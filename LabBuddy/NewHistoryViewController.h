//
//  NewHistoryViewController.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 13/1/20.
//
//

#import <UIKit/UIKit.h>
#import "TypeDefininition.h"

@interface NewHistoryViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

-(id)initWithType:(enum DataType)data_type dataArray:(NSArray*)data_array;

@end
