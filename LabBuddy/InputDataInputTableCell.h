//
//  InputDataInputTableCell.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/12/25.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface InputDataInputTableCell : UITableViewCell {
	IBOutlet UILabel * formal_nameL;
	IBOutlet UILabel * cht_nameL;
	IBOutlet UILabel * valueL;
}

@end
