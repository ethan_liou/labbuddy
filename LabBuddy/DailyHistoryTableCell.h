//
//  DailyHistoryTableCell.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/12/24.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Entry.h"

@interface DailyHistoryTableCell : UITableViewCell {
	IBOutlet UILabel * cht_nameL;
	IBOutlet UILabel * formal_nameL;
	IBOutlet UILabel * usageL;
}

- (void)loadEntry:(Entry*)e;

@end
