//
//  MyDetailPicker.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/12/30.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MyDetailPicker.h"


@implementation MyDetailPicker
@synthesize entryIndex,output_postfix_;
-(id) init{
	self = [super init];
	if(self){
		self.dataSource = self;
		self.delegate = self;
		self.showsSelectionIndicator = YES;
		
	}
	return self;
}

-(id) initWithCat:(NSArray*)data index:(NSArray *)indexes{
	self = [self init];
	if(self){
		data_ = [[NSArray alloc] initWithArray:data];
		indexes_ = [[NSArray alloc] initWithArray:indexes];
		entryIndex = -1;
	}
	return self;
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
	if(view != nil){
		UILabel * label = (UILabel *)view;
		UIFont *font = [UIFont boldSystemFontOfSize:17];
		if([[self pickerView:pickerView titleForRow:row forComponent:component] length] > 15){
			font = [UIFont boldSystemFontOfSize:14];
		}
		label.font = font;
		label.text = [self pickerView:pickerView titleForRow:row forComponent:component];
		label.textColor = row == 0 ? [UIColor redColor] : [UIColor blackColor];
		return label;
	}
	else{
		NSInteger width = 270 / [data_ count];
		UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, 45)];
		[label setTextAlignment:UITextAlignmentCenter];
		label.opaque=NO;
		label.numberOfLines = 2;
		label.backgroundColor=[UIColor clearColor];
		label.textColor = row == 0 ? [UIColor redColor] : [UIColor blackColor];
		UIFont *font = [UIFont boldSystemFontOfSize:17];
		if([[self pickerView:pickerView titleForRow:row forComponent:component] length] > 15){
			font = [UIFont boldSystemFontOfSize:14];
		}
		label.font = font;
		label.text = [self pickerView:pickerView titleForRow:row forComponent:component];
		return label;
	}
	return nil;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
	return 50;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
	if (component == 1) {
		if ([pickerView numberOfRowsInComponent:1] == 0) {
			return;
		}
	}
	if ([data_ count] != 1 && component == 0) {
		NSMutableArray * arr = [[NSMutableArray alloc] init];
		for(NSString * str in [data_ objectAtIndex:0]){
      if([arr containsObject:str])continue;
			[arr addObject:str];
		}		
		key_ = [arr objectAtIndex:row];
		[self reloadComponent:1];
    [self selectRow:0 inComponent:1 animated:NO];
	}  
  else if(component == 1){
    NSMutableArray * array = [[NSMutableArray alloc] initWithObjects:[[data_ objectAtIndex:1] objectAtIndex:0],nil];
		for (int i = 1; i < [[data_ objectAtIndex:1] count]; i++) {
			if ([[[data_ objectAtIndex:0] objectAtIndex:i] isEqualToString:key_]) {
				[array addObject:[[data_ objectAtIndex:1] objectAtIndex:i]];
			}
		}
		value_ = [array objectAtIndex:row];
  }
  else if(component == 0){
    NSMutableArray * arr = [[NSMutableArray alloc] init];
		for(NSString * str in [data_ objectAtIndex:0]){
			[arr addObject:str];
		}
		key_ = [arr objectAtIndex:row];
  }
  AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
  for (NSNumber * number in indexes_){
    NSMutableArray * notEmptyField = [[NSMutableArray alloc] init];
    Entry * entry = [delegate.allEntrys objectAtIndex:[number intValue]];    
    if([entry.source length]!=0){
      [notEmptyField addObject:entry.source];
    }
    if([entry.age length]!=0){
      [notEmptyField addObject:entry.age];        
    }
    if([entry.time length]!=0){
      [notEmptyField addObject:entry.time];
    }
    if([entry.pose length]!=0){
      [notEmptyField addObject:entry.pose];
    }
    if([entry.mc length]!=0){
      [notEmptyField addObject:entry.mc];
    }
    if([entry.pregnant length]!=0){
      [notEmptyField addObject:entry.pregnant];
    }
    if([entry.food length]!=0){
      [notEmptyField addObject:entry.food];
    }
    if([entry.medicine length]!=0){
      [notEmptyField addObject:entry.medicine];
    }
    if ([data_ count] == 1 && [notEmptyField containsObject:key_]) {
      entryIndex = number.intValue;
		output_postfix_ = [NSString stringWithFormat:@"(%@:%@)",
						   [[data_ objectAtIndex:0] objectAtIndex:0],
						   key_];
      return;
    }
    else if([data_ count] == 2 && 
			[notEmptyField containsObject:key_] && 
			([notEmptyField containsObject:value_] || [pickerView numberOfRowsInComponent:1] == 0)){
      entryIndex = number.intValue;
		NSString * two_postfix = nil;
		if([pickerView numberOfRowsInComponent:1] == 0){
			two_postfix = @"";
		}
		else{
			two_postfix = [NSString stringWithFormat:@",%@",
						   value_];
		}
		output_postfix_ = [NSString stringWithFormat:@"(%@%@)",
						   key_,two_postfix];
      return;
    }
  }  
  entryIndex = -1;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
	return [data_ count];
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
	if ([data_ count] == 1) {
		return [[data_ objectAtIndex:component] count];
	}
	if(component == 0){
		NSMutableArray * arr = [[NSMutableArray alloc] init];
		for(NSString * str in [data_ objectAtIndex:component]){
			if (![arr containsObject:str]) {
				[arr addObject:str];
			}
		}
		return [arr count];
	}
	else {
		NSMutableArray * array = [[NSMutableArray alloc] initWithObjects:[[data_ objectAtIndex:1] objectAtIndex:0],nil];
		for (int i = 1; i < [[data_ objectAtIndex:1] count]; i++) {
			if ([[[data_ objectAtIndex:0] objectAtIndex:i] isEqualToString:key_] &&
				[[[data_ objectAtIndex:1] objectAtIndex:i] length] != 0) {
				[array addObject:[[data_ objectAtIndex:1] objectAtIndex:i]];
			}
		}
		return [array count] == 1 ? 0 : [array count];
	}
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
	if ([data_ count] == 1) {
		return [[data_ objectAtIndex:component] objectAtIndex:row];
	}
	if(component == 0){
		NSMutableArray * arr = [[NSMutableArray alloc] init];
		for(NSString * str in [data_ objectAtIndex:component]){
			if (![arr containsObject:str]) {
				[arr addObject:str];
			}
		}
		return [arr objectAtIndex:row];
	}
	else {
		NSMutableArray * array = [[NSMutableArray alloc] initWithObjects:[[data_ objectAtIndex:1] objectAtIndex:0],nil];
		for (int i = 1; i < [[data_ objectAtIndex:1] count]; i++) {
			if ([[[data_ objectAtIndex:0] objectAtIndex:i] isEqualToString:key_]) {
				[array addObject:[[data_ objectAtIndex:1] objectAtIndex:i]];
			}
		}
		return [array objectAtIndex:row];
	}

}

@end
