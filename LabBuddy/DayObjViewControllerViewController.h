//
//  DayObjViewControllerViewController.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 12/7/10.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DayObjViewControllerViewController : UIViewController

enum DateType{
	DATETYPE_NORMAL,DATETYPE_PERIOD,DATETYPE_DANGEROUS,DATETYPE_OVULATION,DATETYPE_SEX,DATETYPE_OUT_OF_BOUND
};

@property (nonatomic, strong) IBOutlet UILabel * day_lbl_;
@property (nonatomic, strong) IBOutlet UIImageView * icon_iv_;
@property (nonatomic, strong) IBOutlet UIButton * btn_;
@property (nonatomic, weak) IBOutlet UILabel * lbl_;

@property (nonatomic) NSInteger year_;
@property (nonatomic) NSInteger month_;
@property (nonatomic) NSInteger day_;
@property (nonatomic) enum DateType date_type_;
@property (nonatomic) BOOL is_selected_;

@property (nonatomic, strong) NSArray * basic_history_arr_;
@property (nonatomic, strong) NSArray * history_arr_;


-(void)deselectDay;
-(void)selectDay;

-(void)setYear:(NSInteger)year Month:(NSInteger)month Day:(NSInteger)day;
-(void)setBasicHistory:(NSArray*)basicArr History:(NSArray*)arr;
-(void)fillDataWithTargetMonth:(NSInteger)month;




@end
