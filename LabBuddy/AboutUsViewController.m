//
//  AboutUsViewController.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/12/17.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "AboutUsViewController.h"
//#import "GANTracker.h"

@implementation AboutUsViewController

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	[fans setTitle:NSLocalizedString(@"Visit Fan Page", @"Visit Fan Page") forState:UIControlStateNormal];
//	NSError * error;
//	if (![[GANTracker sharedTracker] trackPageview:@"/aboutus"
//										 withError:&error]) {
//	}
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (IBAction)fans{

	NSString* safariUrl = @"https://www.facebook.com/WeCareU";
	NSString* fbUrl = @"fb://page/164362826997884";
	NSString* app = nil;
	if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:fbUrl]]) {
		app = @"fb";
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:fbUrl]];
	}
	else {
		app = @"browser";
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:safariUrl]];
	}	
//	NSError * error;
//	if (![[GANTracker sharedTracker] trackEvent:@"user"
//										 action:@"fans"
//										  label:app
//										  value:1
//									  withError:&error]) {
//	}
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

@end
