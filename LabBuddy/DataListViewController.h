//
//  DataListViewController.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 12/11/24.
//
//

#import <UIKit/UIKit.h>
#import "TypeDefininition.h"

@interface DataListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

-(id)initWithType:(enum DataType)dataType Object:(NSObject*)obj;

@end
