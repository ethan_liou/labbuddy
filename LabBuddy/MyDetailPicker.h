//
//  MyDetailPicker.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/12/30.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "Entry.h"

@interface MyDetailPicker : UIPickerView<UIPickerViewDelegate,UIPickerViewDataSource>{
	NSArray * data_;
	NSString * key_;
  NSString * value_;
  NSArray * indexes_;
  NSInteger entryIndex;
}

@property NSInteger entryIndex;
@property (strong,nonatomic) NSString * output_postfix_;
-(id) initWithCat:(NSArray*)data index:(NSArray*)indexes;

@end
