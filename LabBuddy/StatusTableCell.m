//
//  StatusTableCell.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/12/24.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "StatusTableCell.h"
#import "Utils.h"


@implementation StatusTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code.
    }
    return self;
}

- (void)loadEntry:(Entry*)e{
	headerL.text = [NSString stringWithFormat:@"%@=%@",e.formal_name,e.value];
	suggestTF.text = [NSString stringWithFormat:@"建議專科：%@",e.suggestion];
	//	two
	if ([e.lower_bond length] == 0 || [e.upper_bond length] == 0) {
		if([e.lower_bond length] == 0){
			if ([Utils is:e.upper_bond startWith:e.value]) {
				//	too high
				headerIV.image = [UIImage imageNamed:@"high_header.png"];
				headerL.textColor = [UIColor colorWithRed:91 green:9 blue:9 alpha:1];
				contentL.text = e.red_simple;				
			}
			else{
				//	normal
				headerIV.image = [UIImage imageNamed:@"normal_header.png"];
				headerL.textColor = [UIColor colorWithRed:24 green:50 blue:6 alpha:1];
				contentL.text = e.green_simple;				
			}
		}
		else{
			if ([Utils is:e.lower_bond startWith:e.value]) {
				//	normal
				headerIV.image = [UIImage imageNamed:@"normal_header.png"];
				headerL.textColor = [UIColor colorWithRed:24 green:50 blue:6 alpha:1];
				contentL.text = e.green_simple;								
			}
			else{
				//	lower
				headerIV.image = [UIImage imageNamed:@"low_header.png"];
				headerL.textColor = [UIColor colorWithRed:11 green:7 blue:68 alpha:1];
				contentL.text = e.blue_simple;				
			}
		}
		return;
	}
	if ([e.value doubleValue] > [e.upper_bond doubleValue]) {
		//	higher
		headerIV.image = [UIImage imageNamed:@"high_header.png"];
		headerL.textColor = [UIColor colorWithRed:91 green:9 blue:9 alpha:1];
		contentL.text = e.red_simple;
	}else if ([e.value doubleValue] < [e.lower_bond doubleValue]) {
		//	lower
		headerIV.image = [UIImage imageNamed:@"low_header.png"];
		headerL.textColor = [UIColor colorWithRed:11 green:7 blue:68 alpha:1];
		contentL.text = e.blue_simple;
	}else {
		//	normal
		headerIV.image = [UIImage imageNamed:@"normal_header.png"];
		headerL.textColor = [UIColor colorWithRed:24 green:50 blue:6 alpha:1];
		contentL.text = e.green_simple;
	}
	
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state.
}


@end
