//
//  RefViewController.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 12/2/2.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "RefViewController.h"
//#import "GANTracker.h"

@implementation RefViewController
@synthesize controller,suggestion;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)backAction{
	[self.navigationController popViewControllerAnimated:YES];
}



#pragma mark - View lifecycle

- (IBAction)gotoFans{
	NSString* safariUrl = @"https://www.facebook.com/WeCareU";
	NSString* fbUrl = @"fb://page/164362826997884";
	NSString* app = nil;
	if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:fbUrl]]) {
		app = @"fb";
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:fbUrl]];
	}
	else {
		app = @"browser";
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:safariUrl]];
	}	
//	NSError * error;
//	if (![[GANTracker sharedTracker] trackEvent:@"user"
//										 action:@"fans"
//										  label:app
//										  value:1
//									  withError:&error]) {
//	}
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
  UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
	button.bounds = CGRectMake(0, 0, 65.0, 30.0);
    [button setBackgroundImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
	[button setTitle:NSLocalizedString(@"Back", @"Back") forState:UIControlStateNormal];
   	button.titleLabel.font = [UIFont boldSystemFontOfSize:14];
	button.titleEdgeInsets = UIEdgeInsetsMake(0, 12, 0, 0);
	[button addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
	
	[fans setTitle:NSLocalizedString(@"Visit Fan Page", @"Visit Fan Page") forState:UIControlStateNormal];
	sugL.text = NSLocalizedString(@"Suggest", @"Suggest");
	refL.text = NSLocalizedString(@"Related Tests", @"Related Tests");
//	NSError * error;
//	if (![[GANTracker sharedTracker] trackPageview:@"/reference"
//										 withError:&error]) {
//	}
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
