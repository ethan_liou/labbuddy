//
//  RefViewController.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 12/2/2.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReferenceTableController.h"
@interface RefViewController : UIViewController{
  IBOutlet ReferenceTableController * controller;
  IBOutlet UILabel * suggestion;  
	IBOutlet UIButton * fans;
	IBOutlet UILabel * sugL;
	IBOutlet UILabel * refL;
}

- (IBAction)gotoFans;
- (void)backAction;
@property(nonatomic,strong) ReferenceTableController * controller;
@property(nonatomic,strong) UILabel * suggestion;

@end
