//
//  HistoryDateValueCell.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 13/1/20.
//
//

#import "HistoryDateValueCell.h"

@implementation HistoryDateValueCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
