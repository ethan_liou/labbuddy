//
//  HistoryCell.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 13/1/21.
//
//

#import <UIKit/UIKit.h>

@interface HistoryCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel * date_l_;
@property (nonatomic, weak) IBOutlet UILabel * value_l_;

@end
