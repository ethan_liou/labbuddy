//
//  Utils.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/10/15.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+(NSInteger)getUserId{
	return 0;
}

+(BOOL)is:(NSString *)stringA startWith:(NSString *)stringB{
	NSRange range = [[stringA lowercaseString] rangeOfString:[stringB lowercaseString]];
	if (range.location == 0) {
		return YES;
	}	
	return NO;
}

+(BOOL)is:(NSString *)stringA contain:(NSString *)stringB{
	NSRange range = [[stringA lowercaseString] rangeOfString:[stringB lowercaseString]];
	if (range.location == NSNotFound) {
		return NO;
	}	
	return YES;
}

+(UIButton*)generateBtn:(NSString *)img_name{
	UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
	UIImage * img = [UIImage imageNamed:img_name];
	[button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[button.titleLabel setFont:[UIFont boldSystemFontOfSize:12]];
	button.bounds = CGRectMake(0,0,img.size.width , img.size.height);
	[button setBackgroundImage:img forState:UIControlStateNormal];
	return button;
}

@end
