//
//  LabBuddyViewController.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/10/12.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface LabBuddyViewController : UIViewController <UIActionSheetDelegate>{
	IBOutlet UIButton * maleBtn;
	IBOutlet UIButton * femaleBtn;
	IBOutlet UIButton * boyBtn;	
	IBOutlet UIButton * girlBtn;
	IBOutlet UIButton * dateBtn;
	IBOutlet UILabel * maleL;
	IBOutlet UILabel * femaleL;
	IBOutlet UILabel * boyL;
	IBOutlet UILabel * girlL;
	IBOutlet UILabel * boySubL;
	IBOutlet UILabel * girlSubL;
	IBOutlet UILabel * chooseL;
	UIDatePicker * datePicker;
}

- (IBAction)selectCategory:(id)sender;
- (IBAction)selectDate;

@end

