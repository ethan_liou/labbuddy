//
//  MyHistoryViewController.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/12/17.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MyHistoryViewController.h"
#import "DBHelper.h"
#import "DailyHistoryViewController.h"
//#import "GANTracker.h"

@implementation MyHistoryViewController

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

-(void)toggleEditing{
	UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
	button.bounds = CGRectMake(0, 0, 65.0, 30.0);
	editing = !editing;
	NSString * imageName = editing ? @"done_btn.png" : @"edit_btn.png";
	NSString * title = editing ? NSLocalizedString(@"Done", @"Done") : NSLocalizedString(@"Edit", @"Edit");
	[button setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
	[button setTitle:title forState:UIControlStateNormal];
	button.titleLabel.font = [UIFont boldSystemFontOfSize:14];
	[button addTarget:self action:@selector(toggleEditing) forControlEvents:UIControlEventTouchUpInside];
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
  [historyTV reloadData];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
//	NSError * error;
//	if (![[GANTracker sharedTracker] trackPageview:@"/history"
//										 withError:&error]) {
//	}
	
	DBHelper * helper = [DBHelper newInstance];
	NSLog(@"%@",[helper queryDate]);
	dateString = [[NSMutableArray alloc] initWithArray:[helper queryDate]];
	UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
	button.bounds = CGRectMake(0, 0, 65.0, 30.0);
	editing = NO;
	NSString * imageName = editing ? @"done_btn.png" : @"edit_btn.png";
	NSString * title = editing ? NSLocalizedString(@"Done", @"Done") : NSLocalizedString(@"Edit", @"Edit");
	[button setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
	[button setTitle:title forState:UIControlStateNormal];
	button.titleLabel.font = [UIFont boldSystemFontOfSize:14];
	[button addTarget:self action:@selector(toggleEditing) forControlEvents:UIControlEventTouchUpInside];
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
	[historyTV reloadData];
}

- (void)viewDidAppear:(BOOL)animated{
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

//	tableview
- (void) deleteOneDayHistory:(id)sender{
  // remove history
  DBHelper * helper = [DBHelper newInstance];
  [helper removeDateHistory:[dateString objectAtIndex:((UIButton*)sender).tag]];
  [dateString removeObjectAtIndex:((UIButton*)sender).tag];
  [historyTV reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [dateString count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"History";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier] ;
    }
	
	cell.textLabel.text = [dateString objectAtIndex:indexPath.row];
  if (editing) {
    UIButton * deleteBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 47, 23)];
	  [deleteBtn setBackgroundImage:[UIImage imageNamed:@"delete_btn.png"] forState:UIControlStateNormal];
	  [deleteBtn setTitle:NSLocalizedString(@"Delete", @"Delete") forState:UIControlStateNormal];
	  deleteBtn.titleLabel.font = [UIFont systemFontOfSize:12];  
    [deleteBtn addTarget:self action:@selector(deleteOneDayHistory:) forControlEvents:UIControlEventTouchUpInside];
    deleteBtn.tag = indexPath.row;
    cell.accessoryView = deleteBtn;
  }
  else
    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow.png"]];
  return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	DailyHistoryViewController * dhvc = [[DailyHistoryViewController alloc] initWithDate:[dateString objectAtIndex:indexPath.row]];
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
	[self.navigationController pushViewController:dhvc animated:YES];
}

//	table


@end
