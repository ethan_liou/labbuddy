//
//  DayObjViewControllerViewController.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 12/7/10.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "DayObjViewControllerViewController.h"

@interface DayObjViewControllerViewController ()

@end

@implementation DayObjViewControllerViewController

@synthesize day_lbl_,icon_iv_,date_type_,is_selected_,btn_,day_,month_,year_,basic_history_arr_,history_arr_,lbl_;

-(void)setColor{
	if(date_type_ == DATETYPE_OUT_OF_BOUND){
		btn_.enabled = NO;
	}
	else{
		btn_.enabled = YES;
	}
	switch (date_type_) {
		case DATETYPE_DANGEROUS:
			[self.view setBackgroundColor:[UIColor colorWithRed:219.0/255 green:228.0/255 blue:244.0/255 alpha:1.0]];
			[day_lbl_ setTextColor:[UIColor colorWithRed:65.0/255 green:86.0/255 blue:126.0/255 alpha:1.0]];
			[icon_iv_ setHidden:NO];
			[icon_iv_ setImage:[UIImage imageNamed:@"danger_icon.png"]];
			break;
		case DATETYPE_OVULATION:
			[self.view setBackgroundColor:[UIColor colorWithRed:165.0/255 green:165.0/255 blue:223.0/255 alpha:1.0]];
			[day_lbl_ setTextColor:[UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0]];
			[icon_iv_ setHidden:NO];
			[icon_iv_ setImage:[UIImage imageNamed:@"ovulation_icon.png"]];
			break;
		case DATETYPE_PERIOD:
			[self.view setBackgroundColor:[UIColor colorWithRed:164.0/255 green:117.0/255 blue:118.0/255 alpha:1.0]];
			[day_lbl_ setTextColor:[UIColor colorWithRed:250.0/255 green:230.0/255 blue:234.0/255 alpha:1.0]];
			[icon_iv_ setHidden:NO];
			[icon_iv_ setImage:[UIImage imageNamed:@"danger_icon.png"]];

			break;			
		case DATETYPE_OUT_OF_BOUND:
			[self.view setBackgroundColor:[UIColor clearColor]];
			[day_lbl_ setTextColor:[UIColor lightGrayColor]];
			[icon_iv_ setHidden:YES];
			break;
		case DATETYPE_SEX:
		case DATETYPE_NORMAL:
			[self.view setBackgroundColor:[UIColor clearColor]];
			[day_lbl_ setTextColor:[UIColor colorWithRed:96.0/255 green:96.0/255 blue:96.0/255 alpha:1.0]];
			[icon_iv_ setHidden:YES];
			break;
		default:
			break;
	}
}

-(void)deselectDay{
	is_selected_ = NO;
	[self setColor];
}

-(void)selectDay{
	is_selected_ = YES;
	[self.view setBackgroundColor:[UIColor lightGrayColor]];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	[self setColor];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)setYear:(NSInteger)year Month:(NSInteger)month Day:(NSInteger)day{
	year_ = year;
	month_ = month;
	day_ = day;
}

-(void)setBasicHistory:(NSArray*)basicArr History:(NSArray*)arr{
	basic_history_arr_ = basicArr;
	history_arr_ = arr;
}

-(void)fillDataWithTargetMonth:(NSInteger)month{
	if(month != month_){
		date_type_ =  DATETYPE_OUT_OF_BOUND;
	}
	else{
		date_type_ = DATETYPE_NORMAL;
	}
	day_lbl_.text = [NSString stringWithFormat:@"%d",day_];
	[self setColor];
	
	// fill history
	if(basic_history_arr_ != nil || history_arr_ != nil){
		[lbl_ setHidden:NO];
	}
	else{
		[lbl_ setHidden:YES];
	}
}

@end
