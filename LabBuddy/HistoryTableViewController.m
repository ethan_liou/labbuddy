//
//  HistoryTableViewController.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/12/25.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "HistoryTableViewController.h"
#import "Entry.h"
#import "DetailViewController.h"
#import "DBHelper.h"

@implementation HistoryTableViewController


#pragma mark -
#pragma mark View lifecycle

-(void)setEntrys:(NSMutableArray*)es{
	entrys = [[NSMutableArray alloc] initWithArray:es];
	[self.tableView reloadData];
}

-(void)setParent:(UIViewController*)controller{
	parentController = controller;
}

/*
- (void)viewDidLoad {
    [super viewDidLoad];

    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
*/

/*
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
*/
/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
	return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
  NSLog(@"His Count %d",[entrys count]);
	return [entrys count];
}

-(void)setDateString:(NSString*)str{
  dateString = str;
}

-(void)setEdit:(BOOL)edit{
  isEdit = edit;
  [self.tableView reloadData];
}

-(void)deleteHistory:(id)sender{
  // remove history
  DBHelper * helper = [DBHelper newInstance];
  Entry * e = [entrys objectAtIndex:((UIButton*)sender).tag];
  [helper removeOneHistory:dateString index:e.index category:e.category];
  [entrys removeObject:e];
  [self.tableView reloadData];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
	static NSString *CellIdentifier = @"DailyHistoryTableCell";
	cell = (DailyHistoryTableCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
        [[NSBundle mainBundle] loadNibNamed:@"DailyHistoryTableCell" owner:self options:nil];
    }
	[cell loadEntry:[entrys objectAtIndex:indexPath.row]];
  if (isEdit) {
    UIButton * deleteBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 47, 23)];
	  [deleteBtn setBackgroundImage:[UIImage imageNamed:@"delete_btn.png"] forState:UIControlStateNormal];
	  [deleteBtn setTitle:NSLocalizedString(@"Delete", @"Delete") forState:UIControlStateNormal];
	  deleteBtn.titleLabel.font = [UIFont systemFontOfSize:12];  
    [deleteBtn addTarget:self action:@selector(deleteHistory:) forControlEvents:UIControlEventTouchUpInside];
    deleteBtn.tag = indexPath.row;
    cell.accessoryView = deleteBtn;
  }
  else
    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow.png"]];
	return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
	Entry * entry = [entrys objectAtIndex:indexPath.row];
	DetailViewController * dvc = [[DetailViewController alloc] initWithEntry:entry];
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
	[parentController.navigationController pushViewController:dvc animated:YES];
}


#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}


@end

