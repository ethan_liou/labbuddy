//
//  BasicDataInputViewController.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 12/5/29.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BasicDataInputViewController : UIViewController<UITextFieldDelegate>{
	IBOutlet UIScrollView * data_sv_;
	IBOutlet UIView * woman_v_;
	IBOutlet UIImageView * footer_iv_;
	IBOutlet UIImageView * body_iv_;	
	NSMutableArray * tv_arr_;
}

-(IBAction)hideKeyboard;
-(IBAction)nextStep;

@end
