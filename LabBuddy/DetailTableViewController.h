//
//  DetailTableViewController.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/10/14.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StatusTableCell.h"

@interface DetailTableViewController : UITableViewController {
	IBOutlet StatusTableCell * cell;
}

-(void)backAction;

@end
