//
//  DatePickerViewController.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 12/4/7.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DatePickerViewController : UIViewController{
	IBOutlet UIDatePicker * picker;
	IBOutlet UIButton * ok;
}

@end
