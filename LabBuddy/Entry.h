//
//  Entry.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/10/14.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Entry : NSObject {
	NSString * formal_name;
	NSString * chinese_name;
	NSString * source;
	NSString * age;
	NSString * time;
	NSString * pose;
	NSString * mc;
	NSString * pregnant;
	NSString * food;
	NSString * medicine;
	NSString * usage;
	NSString * unit;
	NSString * upper_bond;
	NSString * lower_bond;
	NSString * red_detail;
	NSString * red_simple;
	NSString * green_detail;
	NSString * green_simple;
	NSString * blue_detail;
	NSString * blue_simple;
	NSString * reference;
	NSString * plus;
	NSString * eng_sym;
	NSString * cht_sym;
	NSString * suggestion;
	NSString * value;
  NSInteger category;
  NSInteger index;
}

@property NSInteger category;
@property NSInteger index;
@property(nonatomic,strong) NSString * formal_name;
@property(nonatomic,strong) NSString * chinese_name;
@property(nonatomic,strong) NSString * source;
@property(nonatomic,strong) NSString * age;
@property(nonatomic,strong) NSString * time;
@property(nonatomic,strong) NSString * pose;
@property(nonatomic,strong) NSString * mc;
@property(nonatomic,strong) NSString * pregnant;
@property(nonatomic,strong) NSString * food;
@property(nonatomic,strong) NSString * medicine;
@property(nonatomic,strong) NSString * usage;
@property(nonatomic,strong) NSString * unit;
@property(nonatomic,strong) NSString * upper_bond;
@property(nonatomic,strong) NSString * lower_bond;
@property(nonatomic,strong) NSString * red_detail;
@property(nonatomic,strong) NSString * red_simple;
@property(nonatomic,strong) NSString * green_detail;
@property(nonatomic,strong) NSString * green_simple;
@property(nonatomic,strong) NSString * blue_detail;
@property(nonatomic,strong) NSString * blue_simple;
@property(nonatomic,strong) NSString * reference;
@property(nonatomic,strong) NSString * plus;
@property(nonatomic,strong) NSString * eng_sym;
@property(nonatomic,strong) NSString * cht_sym;
@property(nonatomic,strong) NSString * suggestion;
@property(nonatomic,strong) NSString * value;

-(NSString*)getCategory;
-(id)initWithArray:(NSArray*)array;

@end
