//
//  HistoryTableViewController.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/12/25.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DailyHistoryTableCell.h"

@interface HistoryTableViewController : UITableViewController {
	IBOutlet DailyHistoryTableCell * cell;
  NSString * dateString;
	NSMutableArray * entrys;
	UIViewController * parentController;
  BOOL isEdit;
}

-(void)setParent:(UIViewController*)controller;
-(void)setEntrys:(NSMutableArray*)es;
-(void)setDateString:(NSString*)str;
-(void)setEdit:(BOOL)edit;
-(void)deleteHistory:(id)sender;

@end
