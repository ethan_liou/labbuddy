//
//  CalendarObj.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 12/7/7.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "DayObjViewControllerViewController.h"
#import "CalendarObj.h"
#import "DBHelper.h"

@implementation CalendarObj

@synthesize year_,month_,day_obj_arr_,basic_history_dict_,history_dict_;

// 1,1(0) ~ 7,2(36)
-(void)generateMonth{
	BOOL first_time = NO;
	if (day_obj_arr_ == nil) {
		first_time = YES;
		day_obj_arr_ = [[NSMutableArray alloc] initWithCapacity:37];
	}
	
	basic_history_dict_ = [[DBHelper newInstance] queryBasicHistoryByYear:year_ AndMonth:month_];
	history_dict_ = [[DBHelper newInstance] queryHistoryByYear:year_ AndMonth:month_];
	NSString *first_day = [NSString stringWithFormat:@"%04d%02d01",year_,month_];
	NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
	[dateFormat setDateFormat:@"yyyyMMdd"];
	NSDate *date = [dateFormat dateFromString:first_day]; 

	//	calculate first day in calendar
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *comps = [gregorian components:NSWeekdayCalendarUnit fromDate:date];
	NSInteger weekday = comps.weekday;
	NSDate * first_calendar_date = [NSDate dateWithTimeInterval:-60*60*24*(weekday-1) sinceDate:date];
	for (int i = 0 ; i < 37; i++) {
		NSDate * d = [NSDate dateWithTimeInterval:60*60*24*i sinceDate:first_calendar_date];
		comps = [gregorian components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:d];
		NSString * dateStr = [NSString stringWithFormat:@"%04d/%02d/%02d",comps.year,comps.month,comps.day];
		DayObjViewControllerViewController * vc = nil;
		if (first_time) {
			vc = [[DayObjViewControllerViewController alloc] initWithNibName:@"DayObjViewControllerViewController" bundle:nil];
			[day_obj_arr_ addObject:vc];
		}
		else{
			vc = [day_obj_arr_ objectAtIndex:i];
		}
		if (vc != nil) {
			[vc setYear:comps.year Month:comps.month Day:comps.day];
			[vc setBasicHistory:[basic_history_dict_ objectForKey:dateStr] History:[history_dict_ objectForKey:dateStr]];
		}
	}
}

@end
