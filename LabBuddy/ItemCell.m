//
//  ItemCell.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/12/22.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
// aaa
#import "ItemCell.h"


@implementation ItemCell

- (void) setDataWithEntry:(Entry *)entry{
	NSLog(@"Entry %@",entry);
	cht_nameL.text = entry.chinese_name;
	formal_nameL.text = entry.formal_name;
	valueL.text = entry.value;
	[deleteBtn setTitle:NSLocalizedString(@"Delete", @"Delete") forState:UIControlStateNormal];
}

- (void) setEdit:(BOOL)edit{
	deleteBtn.hidden = !edit;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code.		
    }
    return self;
}

- (void) setId:(NSInteger)rowId{
	deleteBtn.tag = 1000+rowId;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
//    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state.
}

@end
