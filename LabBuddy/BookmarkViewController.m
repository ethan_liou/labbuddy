//
//  BookmarkViewController.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/12/17.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "BookmarkViewController.h"
#import "DBHelper.h"
#import "DetailViewController.h"
//#import "GANTracker.h"

@implementation BookmarkViewController

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/

-(void)toggleEditing{
	UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
	button.bounds = CGRectMake(0, 0, 65.0, 30.0);
	editing = !editing;
	[table reloadData];
//  [controller setEditing:editing];
//  [controller.tableView reloadData];
	NSString * imageName = editing ? @"done_btn.png" : @"edit_btn.png";
	NSString * title = editing ? NSLocalizedString(@"Done", @"Done") : NSLocalizedString(@"Edit", @"Edit");
	[button setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
	[button setTitle:title forState:UIControlStateNormal];
	button.titleLabel.font = [UIFont boldSystemFontOfSize:14];
	[button addTarget:self action:@selector(toggleEditing) forControlEvents:UIControlEventTouchUpInside];
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
  [super viewDidLoad];
//  DBHelper * helper = [DBHelper newInstance];
//	NSMutableArray * entrys = [helper queryBookmark];
//	[controller setParent:self];
//	[controller setEntrys:entrys];	
//  [controller setEditing:editing];
	UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
	button.bounds = CGRectMake(0, 0, 65.0, 30.0);
	NSString * imageName = editing ? @"done_btn.png" : @"edit_btn.png";
	NSString * title = editing ? NSLocalizedString(@"Done", @"Done") : NSLocalizedString(@"Edit", @"Edit");
	[button setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
	[button setTitle:title forState:UIControlStateNormal];
	button.titleLabel.font = [UIFont boldSystemFontOfSize:14];
	[button addTarget:self action:@selector(toggleEditing) forControlEvents:UIControlEventTouchUpInside];
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
	editing = NO;
//	NSError * error;
//	if (![[GANTracker sharedTracker] trackPageview:@"/bookmark"
//										 withError:&error]) {
//	}

}

- (void)viewWillAppear:(BOOL)animated{
	entrys = [[DBHelper newInstance] queryBookmark];
  [table reloadData];
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.5];
	for(UIView *view in self.tabBarController.view.subviews)
	{
		if([view isKindOfClass:[UITabBar class]]){
			[view setFrame:CGRectMake(view.frame.origin.x, 480-49, view.frame.size.width, view.frame.size.height)];
		} 
		else {
			[view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 480-49)];
		}
	}
	[UIView commitAnimations];		
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
  // Return the number of sections.
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  // Return the number of rows in the section.
  return [entrys count];
}

- (void) deleteBookmark:(id)sender{
  DBHelper * helper = [DBHelper newInstance];
  Entry * e = [entrys objectAtIndex:((UIButton*)sender).tag];
  [helper removeBookmarkWithCategory:e.category Index:e.index];
  [entrys removeObject:e];
  [table reloadData];  
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *CellIdentifier = @"BookmarkCell";
	cell = (DailyHistoryTableCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
    [[NSBundle mainBundle] loadNibNamed:@"DailyHistoryTableCell" owner:self options:nil];
  }
	[cell loadEntry:[entrys objectAtIndex:indexPath.row]];
  if (editing) {
    UIButton * deleteBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 47, 23)];
    [deleteBtn setBackgroundImage:[UIImage imageNamed:@"delete_btn.png"] forState:UIControlStateNormal];
	[deleteBtn setTitle:NSLocalizedString(@"Delete", @"Delete") forState:UIControlStateNormal];
	deleteBtn.titleLabel.font = [UIFont systemFontOfSize:12];  
    [deleteBtn addTarget:self action:@selector(deleteBookmark:) forControlEvents:UIControlEventTouchUpInside];
    deleteBtn.tag = indexPath.row;
    cell.accessoryView = deleteBtn;
  }
  else
    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow.png"]];
	return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }   
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  // Navigation logic may go here. Create and push another view controller.
	Entry * entry = [entrys objectAtIndex:indexPath.row];
	DetailViewController * dvc = [[DetailViewController alloc] initWithEntry:entry];
	[self.navigationController pushViewController:dvc animated:YES];
}

@end
