//
//  PWViewController.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 12/5/27.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "PWViewController.h"

@interface PWViewController ()

@end

@implementation PWViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	[pw_tf_ becomeFirstResponder];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)fillTF:(NSInteger)idx{
	switch (idx) {
		case 0:
			pw_tf_1.text = @"*";
			break;
		case 1:
			pw_tf_2.text = @"*";
			break;
		case 2:
			pw_tf_3.text = @"*";
			break;
		case 3:
			pw_tf_4.text = @"*";
			break;
		default:
			break;
	}
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
	if (newLength > 4) {
		return NO;
	}
	
	pw_tf_1.text = @"";
	pw_tf_2.text = @"";
	pw_tf_3.text = @"";
	pw_tf_4.text = @"";	
	for (int i = 0 ; i < newLength; ++i) {
		[self fillTF:i];
	}
	return YES;
}

@end
