//
//  MyHistoryViewController.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/12/17.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MyHistoryViewController : UIViewController <UITableViewDelegate,UITableViewDataSource> {
	IBOutlet UITableView * historyTV;
	NSMutableArray * dateString;
  BOOL editing;
}

-(void)toggleEditing;

@end
