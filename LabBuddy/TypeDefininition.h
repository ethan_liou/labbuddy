//
//  TypeDefininition.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 13/1/20.
//
//

#ifndef LabBuddy_TypeDefininition_h
#define LabBuddy_TypeDefininition_h

enum DataType{
	DATA_BASIC_TYPE, DATA_TEST_TYPE
};

enum BasicHistoryType{
	HEIGHT,WEIGHT,WAIST,FAT,BP_H,BP_L
};

#endif
