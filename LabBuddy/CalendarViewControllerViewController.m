//
//  CalendarViewControllerViewController.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 12/7/4.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "DayObjViewControllerViewController.h"
#import "CalendarViewControllerViewController.h"
#import "CalendarObj.h"
#import "NewHistoryViewController.h"

@interface CalendarViewControllerViewController (){
	CalendarObj * calendarObj;
	DayObjViewControllerViewController * selectedDay;
}

@end

@implementation CalendarViewControllerViewController

-(IBAction)detailData:(id)sender{
	if (sender == basic_b_) {
		NewHistoryViewController * nhvc = [[NewHistoryViewController alloc] initWithType:DATA_BASIC_TYPE dataArray:selectedDay.basic_history_arr_];
		[self.navigationController pushViewController:nhvc animated:YES];
	}
	else{
		NewHistoryViewController * nhvc = [[NewHistoryViewController alloc] initWithType:DATA_TEST_TYPE dataArray:selectedDay.history_arr_];
		[self.navigationController pushViewController:nhvc animated:YES];		
	}
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)selectDay:(UIButton*)btn{
	DayObjViewControllerViewController * controller = [calendarObj.day_obj_arr_ objectAtIndex:btn.tag];
	if(selectedDay != nil){
		[selectedDay deselectDay];
	}
	selectedDay = controller;
	[controller selectDay];
	if (controller.basic_history_arr_ == nil) {
		[basic_b_ setEnabled:NO];
	}
	else{
		[basic_b_ setEnabled:YES];
	}
	if (controller.history_arr_ == nil) {
		[history_b_ setEnabled:NO];
	}
	else{
		[history_b_ setEnabled:YES];
	}
}

- (void)loadCalendarWithYear:(NSInteger)year Month:(NSInteger)month{
	if (calendarObj == nil) {
		calendarObj = [[CalendarObj alloc] init];
	}
	calendarObj.year_ = year;
	calendarObj.month_ = month;
	date_l_.text = [NSString stringWithFormat:@"%04d %02d",year,month];
	[calendarObj generateMonth];
	for (int i = 0; i < calendarObj.day_obj_arr_.count; i++) {
		DayObjViewControllerViewController * vc = [calendarObj.day_obj_arr_ objectAtIndex:i];
		NSInteger y = (i / 7) * 39 + (i/7)%2;
		NSInteger x = (i % 7) * 46;
		[vc.view setFrame:CGRectMake(x, y, 45, 39)];
		[day_v_ addSubview:vc.view];
		[vc fillDataWithTargetMonth:month];
		[vc.btn_ addTarget:self action:@selector(selectDay:) forControlEvents:UIControlEventTouchUpInside];
		vc.btn_.tag = i;
		if (vc.day_ == 1 && vc.date_type_ != DATETYPE_OUT_OF_BOUND) {
			[self selectDay:vc.btn_];
		}
	}
}

-(IBAction)nextMonth{
	if(month_ == 12){
		year_ ++;
		month_ = 1;
	}
	else{
		month_ ++;
	}
	[self loadCalendarWithYear:year_ Month:month_];
}

-(IBAction)prevMonth{
	if(month_ == 1){
		year_ --;
		month_ = 12;
	}
	else{
		month_ --;
	}
	[self loadCalendarWithYear:year_ Month:month_];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *components = [gregorian components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:[NSDate date]];
	now_year_ = year_ = [components year];
	now_month_ = month_ = [components month];
	now_day_ = day_ = [components day];
	[self loadCalendarWithYear:year_ Month:month_];
	for (DayObjViewControllerViewController * vc in calendarObj.day_obj_arr_) {
		if(vc.year_ == now_year_ && vc.month_ == now_month_ && vc.day_ == now_day_){
			[self selectDay:vc.btn_];
		}
	}
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
