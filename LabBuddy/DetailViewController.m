//
//  DetailViewController.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/10/15.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DetailViewController.h"
#import "DBHelper.h"
#import "AppDelegate.h"
//#import "GANTracker.h"
#import "Utils.h"

@implementation DetailViewController
@synthesize entry;
- (id)initWithEntry:(Entry *)e{
	self = [super init];
    if (self) {
      // Custom initialization.
      self.entry = e;
      if(e.category == -1 && e.index == -1){
        AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        self.entry.index = [delegate.allEntrys indexOfObject:e];
        self.entry.category = delegate.userCategory;
      }
      bookmarks = [[DBHelper newInstance] queryBookmark];
      isBookmark = [bookmarks containsObject:self.entry];
    }
    return self;
}

-(IBAction)showDetailInfo:(id)sender{
	CGFloat alpha = 0.0f;
	if (sender == showDetailB) {
		alpha = 1.0f;
	}
	else if(sender == detailB){
		alpha = 0.0f;
	}
	[UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
	[detailB setAlpha:alpha];
    [UIView commitAnimations];

}

- (void)assignEntry:(Entry*)e{
  self.entry = e;
  if(e.category == -1 && e.index == -1){
    AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    self.entry.index = [delegate.allEntrys indexOfObject:e];
    self.entry.category = delegate.userCategory;
  }
  bookmarks = [[DBHelper newInstance] queryBookmark];
  isBookmark = [bookmarks containsObject:self.entry];
  [self loadData];
}

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/



- (void)showTab:(BOOL)show{
  if(show){
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    for(UIView *view in self.tabBarController.view.subviews)
    {
      if([view isKindOfClass:[UITabBar class]]){
        [view setFrame:CGRectMake(view.frame.origin.x, 480-49, view.frame.size.width, view.frame.size.height)];
      } 
      else {
        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 480-49)];
      }
    }
    [UIView commitAnimations];	
  }else{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    for(UIView *view in self.tabBarController.view.subviews){
      if([view isKindOfClass:[UITabBar class]])
      {
        [view setFrame:CGRectMake(view.frame.origin.x, 480, view.frame.size.width, view.frame.size.height)];
      } 
      else 
      {
        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 480)];
      }
    }
    [UIView commitAnimations];
  }
}

-(IBAction) showDetail:(id)sender{
	UIButton * btn = (UIButton*)sender;
	three_upperB.hidden = YES;
	three_lowerB.hidden = YES;
	three_normalB.hidden = YES;
	two_upperB.hidden = YES;
	two_normalB.hidden = YES;
	
	switch (btn.tag) {
		case 1:
			high_allB.hidden = NO;
      normal_allB.hidden = YES;
			low_allB.hidden = YES;
      [self fillData:entry.red_detail In:high_allB];
			break;
		case 2:
			high_allB.hidden = YES;
      normal_allB.hidden = YES;
			low_allB.hidden = NO;
      [self fillData:entry.blue_detail In:low_allB];
			break;
    case 3:
      high_allB.hidden = YES;
      normal_allB.hidden = NO;
			low_allB.hidden = YES;
      [self fillData:entry.green_detail In:normal_allB];
      break;
		default:
			break;
	}
}

-(IBAction) hideDetail:(id)sender{
	three_upperB.hidden = NO;
	three_lowerB.hidden = NO;
	three_normalB.hidden = NO;
	two_upperB.hidden = NO;
	two_normalB.hidden = NO;
	high_allB.hidden = YES;
	low_allB.hidden = YES;
	normal_allB.hidden = YES;
}

- (void)backAction{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)addBookmark{
	DBHelper * helper = [DBHelper newInstance];
	AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
	[helper addBookmarkWithCategory:(entry.category == -1)?delegate.userCategory:entry.category Index:(entry.index == -1)?[delegate.allEntrys indexOfObject:entry]:entry.index];
  isBookmark = YES;
  [self setBookmarkBtn];
//	NSError * error;
//	if (![[GANTracker sharedTracker] trackEvent:@"entry"
//										 action:@"add_bookmark"
//										  label:[NSString stringWithFormat:@"%@ (%@)",entry.formal_name,[entry getCategory]]
//										  value:1
//									  withError:&error]) {
//	}
}

- (void)deleteBookmark{
	DBHelper * helper = [DBHelper newInstance];
	[helper removeBookmarkWithCategory:entry.category Index:entry.index];  
  isBookmark = NO;
  [self setBookmarkBtn];
//	NSError * error;
//	if (![[GANTracker sharedTracker] trackEvent:@"entry"
//										 action:@"delete_bookmark"
//										  label:[NSString stringWithFormat:@"%@ (%@)",entry.formal_name,[entry getCategory]]
//										  value:1
//									  withError:&error]) {
//	}

}

- (void)setBookmarkBtn{
  UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
	button.bounds = CGRectMake(0, 0, 65.0, 30.0);
  if(isBookmark){
    [button setImage:[UIImage imageNamed:@"tag_btn_blue.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(deleteBookmark) forControlEvents:UIControlEventTouchUpInside];
  }else{
    [button setImage:[UIImage imageNamed:@"tag_btn.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(addBookmark) forControlEvents:UIControlEventTouchUpInside];    
  }
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];	

}

- (void)fillData:(NSString*)data In:(UIButton*)btn{
  CGRect frame = btn.frame;
  UITextView * tv = [[UITextView alloc] initWithFrame:
                     CGRectMake(25, -5, frame.size.width-15, frame.size.height - 20)];
  tv.text = data;
  tv.backgroundColor = [UIColor clearColor];
  tv.font = [UIFont systemFontOfSize:14];
  tv.editable = NO;
  tv.tag = btn.tag;
  for(UIView * view in btn.subviews){
    if ([view isKindOfClass:[UITextView class]]) {
      [view removeFromSuperview];      
    }
  }
  UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
  
  //modify this number to recognizer number of tap
  [singleTap setNumberOfTapsRequired:1];
  [tv addGestureRecognizer:singleTap];

  [btn addSubview:tv];
}

-(void)handleSingleTap:(id)sender{
  UITapGestureRecognizer * gesture = (UITapGestureRecognizer*)sender;
  UITextView * tv = (UITextView *)gesture.view;
  UIButton * btn = (UIButton*)[tv superview];
  if(tv.tag > 3){
    // hide
    [self hideDetail:btn];
  }
  else{
    [self showDetail:btn];
  }
}

-(void) loadData{
  //	back
	UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
	button.bounds = CGRectMake(0, 0, 65.0, 30.0);
    [button setBackgroundImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
	[button setTitle:NSLocalizedString(@"Back", @"Back") forState:UIControlStateNormal];
   	button.titleLabel.font = [UIFont boldSystemFontOfSize:14];
	button.titleEdgeInsets = UIEdgeInsetsMake(0, 12, 0, 0);
	[button addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
  
	//	tag
  [self setBookmarkBtn];
	
  if ([entry.value length] == 0)
    valueTF.hidden = YES;
  else valueTF.hidden = NO;
  
	if ([entry.lower_bond length] == 0) {
		if ([Utils is:entry.upper_bond startWith:entry.value]) {
			//	too high
			valueTF.background = [UIImage imageNamed:@"red_number_bg.png"];
			valueTF.text = @"異常";
		}
		else{
			//	normal 
			valueTF.background = [UIImage imageNamed:@"green_number_bg.png"];
			valueTF.text = @"正常";
		}
		//	two
		twoV.hidden = NO;
		threeV.hidden = YES;
    
		[self fillData:entry.red_simple In:two_upperB];
		[self fillData:entry.green_simple In:two_normalB];
		[two_upperB setBackgroundImage:[UIImage imageNamed:@"2_positive_bg.png"] forState:UIControlStateNormal];
		[two_normalB setBackgroundImage:[UIImage imageNamed:@"2_normal.png"] forState:UIControlStateNormal];

		two_upperB.tag = 1;
		two_normalB.tag = 3;
	}
	else if ([entry.upper_bond length] == 0) {
		if ([Utils is:entry.lower_bond startWith:entry.value]) {
			//	normal
			valueTF.background = [UIImage imageNamed:@"green_number_bg.png"];
			valueTF.text = @"正常";
		}
		else{
			//	too low 
			valueTF.background = [UIImage imageNamed:@"blue_number_bg.png"];
			valueTF.text = @"異常";
		}

		//	two
		twoV.hidden = NO;
		threeV.hidden = YES;
		
		[self fillData:entry.green_simple In:two_upperB];
		[self fillData:entry.blue_simple In:two_normalB];
		[two_upperB setBackgroundImage:[UIImage imageNamed:@"2_normal.png"] forState:UIControlStateNormal];
		[two_normalB setBackgroundImage:[UIImage imageNamed:@"3_low_bg.png"] forState:UIControlStateNormal];
		two_upperB.tag = 3;
		two_normalB.tag = 2;
	}
	else {
		//	three
		twoV.hidden = YES;
		threeV.hidden = NO;
		three_upperL.text = entry.upper_bond;
		three_lowerL.text = entry.lower_bond;
		if ([entry.value doubleValue] > [entry.upper_bond doubleValue]) {
			valueTF.background = [UIImage imageNamed:@"red_number_bg.png"];
		}else if ([entry.value doubleValue] < [entry.lower_bond doubleValue]) {
			valueTF.background = [UIImage imageNamed:@"blue_number_bg.png"];
		}else {
			valueTF.background = [UIImage imageNamed:@"green_number_bg.png"];
		}
		
		//	text	
    [self fillData:entry.red_simple In:three_upperB];
    [self fillData:entry.green_simple In:three_normalB];
    [self fillData:entry.blue_simple In:three_lowerB];    
		valueTF.text = ([entry.value length] > 4)?[entry.value substringToIndex:4]:entry.value;
	}
	formalL.text = entry.formal_name;
	chtL.text = entry.chinese_name;
	funcL.text = entry.usage;
	unitL.text = entry.unit;
  [refB setTitle:[NSString stringWithFormat:@"同時參考: %@",entry.reference] forState:UIControlStateNormal];
  
  
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
  [super viewDidLoad];
	[self loadData];
//	NSError * error;
//	if (![[GANTracker sharedTracker] trackPageview:@"/detailview"
//										 withError:&error]) {
//	}
}

- (IBAction)switchView{
  NSArray *refs = [entry.reference componentsSeparatedByString: @"、"];
  refController = [[RefViewController alloc] init];
  [self.navigationController pushViewController:refController animated:YES];
  refController.controller.refs_ = refs;
  refController.suggestion.text = [NSString stringWithFormat:@"%@",entry.suggestion];
  [refController.controller.tableView reloadData];
//	NSError * error;
//	if (![[GANTracker sharedTracker] trackEvent:@"entry"
//										 action:@"show_reference"
//										  label:[NSString stringWithFormat:@"%@ (%@)",entry.formal_name,[entry getCategory]]
//										  value:1
//									  withError:&error]) {
//	}

//  [view addSubview:refController.tableView];
  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(switchBack:) name:@"switchBack" object:nil];
//
//  [UIView beginAnimations:@"View Flip" context:nil];
//  [UIView setAnimationDuration:1.25];
//  [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
//  [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.view cache:YES];
//  [self.view addSubview:view];
//  [UIView commitAnimations];
}

- (void) switchBack:(NSNotification *) notification{
  [self assignEntry:(Entry*)notification.object];
  [self.navigationController popViewControllerAnimated:YES];
  [[NSNotificationCenter defaultCenter] removeObserver:self];
//  [UIView beginAnimations:@"View Flip" context:nil];
//  [UIView setAnimationDuration:1.25];
//  [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
//  [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.view cache:YES];
//  [refController.view removeFromSuperview];
//  [UIView commitAnimations];
}

-(void)viewWillDisappear:(BOOL)animated{
  if(showTabTimer != nil)
    [showTabTimer invalidate];
}

- (void)changeTab{
  isShow = !isShow;
  [self showTab:isShow];
  if(isShow){
    //  set timer
    showTabTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(changeTab) userInfo:nil repeats:NO];
  }else{
    [showTabTimer invalidate];
  }
}

- (void)viewDidAppear:(BOOL)animated{
  isShow = NO;
  [self showTab:isShow];
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

@end
