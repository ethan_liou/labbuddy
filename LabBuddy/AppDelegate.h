//
//  AppDelegate.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 12/5/28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

//	global data
@property (nonatomic) NSInteger userCategory;	//	1 : male, 2 : female, 3 : boy, 4 : girl
@property (strong, nonatomic) NSMutableArray * inputedData;
@property (strong, nonatomic) NSMutableArray * allEntrys;
@property (strong, nonatomic) NSMutableArray * everyEntry;
@property (strong, nonatomic) NSMutableArray * allSearchTerms;
@property (strong, nonatomic) NSDate * targetDate;

@property (strong, nonatomic) UIWindow *window;
@end
