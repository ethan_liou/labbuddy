//
//  MyNavController.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 12/5/27.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "MyNavController.h"
#import "Utils.h"
@interface MyNavController ()

@end

@implementation MyNavController

BOOL NavShowLandscape = NO;

- (void)orientationChanged:(NSNotification *)notification
{
    UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
    if (UIDeviceOrientationIsLandscape(deviceOrientation) &&
        !NavShowLandscape)
    {
		[self.navigationBar setBackgroundImage:[UIImage imageNamed:@"header_landscape.jpg"] forBarMetrics:UIBarMetricsDefault];
        NavShowLandscape = YES;
    }
    else if (UIDeviceOrientationIsPortrait(deviceOrientation) &&
             NavShowLandscape)
    {
		[self.navigationBar setBackgroundImage:[UIImage imageNamed:@"header.png"] forBarMetrics:UIBarMetricsDefault];
        NavShowLandscape = NO;
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated{
	NavShowLandscape = NO;
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(orientationChanged:)
												 name:UIDeviceOrientationDidChangeNotification
											   object:nil];
	if([self.parentViewController respondsToSelector:@selector(revealToggle:)] &&
	   [self.parentViewController respondsToSelector:@selector(revealGesture:)]){
		UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self.parentViewController action:@selector(revealGesture:)];	
		[self.navigationBar addGestureRecognizer:panGestureRecognizer];
		[self.navigationBar setBackgroundImage:[UIImage imageNamed:@"header.png"] forBarMetrics:UIBarMetricsDefault];	
		
		UIButton * menu_btn = [Utils generateBtn:@"slide_btn_new.png"];
		[menu_btn addTarget:self.navigationController.parentViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
		UIBarButtonItem * item = [[UIBarButtonItem alloc] initWithCustomView:menu_btn];
		self.topViewController.navigationItem.leftBarButtonItem = item;
	}
	else{
		NSLog(@"MyNav Error");
	}
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
