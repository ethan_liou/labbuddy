//
//  DailyHistoryTableCell.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/12/24.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DailyHistoryTableCell.h"


@implementation DailyHistoryTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code.
		
    }
    return self;
}

- (void)loadEntry:(Entry*)e{
	//	1 : male, 2 : female, 3 : boy, 4 : girl
	NSArray * arr = [NSArray arrayWithObjects:@"",@"成年男性",@"成年女性",@"男孩",@"女孩", nil];
	cht_nameL.text = [NSString stringWithFormat:@"%@ (%@)",e.chinese_name,[arr objectAtIndex:e.category]];
	formal_nameL.text = e.formal_name;
	usageL.text = e.usage;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state.
}



@end
