//
//  SearchTerm.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/12/19.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SearchTerm : NSObject {
	NSString * formal_name;
	NSString * chinese_name;
	NSString * eng_sym;
	NSString * cht_sym;
	NSString * stringToShow;
	NSMutableArray * entryIndexes;
}

@property (nonatomic, strong) NSString * formal_name;
@property (nonatomic, strong) NSString * stringToShow;
@property (nonatomic, strong) NSMutableArray * entryIndexes;

-(id)initWithArray:(NSArray*)array;
-(void)addIndex:(NSInteger)index;
-(BOOL)match:(NSString*)prefix;

@end
