//
//  MenuTableViewCell.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 12/5/28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableViewCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UILabel * lbl_;
@property (nonatomic,strong) IBOutlet UIImageView * img_;

@end
