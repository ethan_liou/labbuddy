//
//  BookmarkViewController.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/12/17.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DailyHistoryTableCell.h"

@interface BookmarkViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>{
	
	IBOutlet UITableView * table;
  IBOutlet DailyHistoryTableCell * cell;
  BOOL editing;
  NSMutableArray * entrys;
}

-(void)toggleEditing;

@end
