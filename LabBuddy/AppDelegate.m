//
//  AppDelegate.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 12/5/28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "ZUUIRevealController.h"
//#import "GANTracker.h"
#import "PWViewController.h"
#import "MenuTableViewController.h"
#import "MyNavController.h"
#import "LabBuddyViewController.h"
#import "DBHelper.h"
#import "CalendarObj.h"

@implementation AppDelegate

@synthesize inputedData,userCategory,allEntrys,everyEntry,allSearchTerms,targetDate;
@synthesize window = _window;

static const NSInteger kGANDispatchPeriodSec = 10;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
	
	LabBuddyViewController * lbvc = [[LabBuddyViewController alloc] initWithNibName:@"LabBuddyViewController" bundle:nil];
	MyNavController * nlbvc = [[MyNavController alloc] initWithRootViewController:lbvc];
	MenuTableViewController * menu = [[MenuTableViewController alloc] initWithNibName:@"MenuTableViewController" bundle:nil];
	ZUUIRevealController * controller = [[ZUUIRevealController alloc] initWithFrontViewController:nlbvc rearViewController:menu];
	self.window.rootViewController = controller;
    [self.window makeKeyAndVisible];
//    //	load google analytics
//	[[GANTracker sharedTracker] startTrackerWithAccountID:@"UA-29870139-1"
//										   dispatchPeriod:kGANDispatchPeriodSec
//												 delegate:nil];
//	
//	NSError * error;
//	if (![[GANTracker sharedTracker] trackPageview:@"/app_entry_point"
//										 withError:&error]) {
//	}
	
	targetDate = nil;
    allEntrys = nil;
	inputedData = [[NSMutableArray alloc] init];
	
	//	load all
	everyEntry = [[NSMutableArray alloc] init];
	[everyEntry addObject:[NSArray arrayWithObject:@"null"]];	//	useless
	DBHelper * dbHelper = [DBHelper newInstance];
	for (int i = 0; i < 4; i++) {
		NSMutableArray * tempArray = [[NSMutableArray alloc] init];
		NSString * tblName;
		switch (i) {
			case 0:
				tblName = @"male";
				break;
			case 1:
				tblName = @"female";
				break;
			case 2:
				tblName = @"boy";
				break;
			case 3:
				tblName = @"girl";
				break;				
			default:
				break;
		}
		NSString * sql = [NSString stringWithFormat:@"SELECT * FROM %@",tblName];
		sqlite3_stmt *statement = [dbHelper executeQuery:sql];
		while(sqlite3_step(statement) == SQLITE_ROW){
			Entry * entry = [dbHelper makeEntry:statement];
			[tempArray addObject:entry];
		}
		[everyEntry addObject:tempArray];
	}
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
	// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
