//
//  InputDataInputTableCell.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/12/25.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "InputDataInputTableCell.h"


@implementation InputDataInputTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code.
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state.
}


@end
