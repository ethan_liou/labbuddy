//
//  InputViewController.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/10/12.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "InputViewController.h"
#import "DetailTableViewController.h"
//#import "GANTracker.h"
#import "DBHelper.h"

@implementation UITextField (custom)

- (CGRect)textRectForBounds:(CGRect)bounds {
	return CGRectMake(bounds.origin.x + 10, bounds.origin.y + 8,
					  bounds.size.width - 20, bounds.size.height - 16);
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
	return [self textRectForBounds:bounds];
}

@end



@implementation InputViewController

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */

- (IBAction)selectValueTF{
	if ([nowEntry.lower_bond length] == 0 || [nowEntry.upper_bond length] == 0) {
		//	two
		NSString * s = [nowEntry.lower_bond length] == 0 ? nowEntry.upper_bond : nowEntry.lower_bond;
		NSArray *values = [s componentsSeparatedByString: @"、"];
		UIActionSheet * sheet = [[UIActionSheet alloc] initWithTitle:@"檢驗數值" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil, nil];
		for(NSString * str in values){
			[sheet addButtonWithTitle:str];
		}
		[sheet addButtonWithTitle:@"取消"];
		[sheet setCancelButtonIndex:2];
		[sheet showInView:self.view];
	}
}

-(void)toggleEditing{
	UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
	button.bounds = CGRectMake(0, 0, 65.0, 30.0);
	editing = !editing;
	NSString * imageName = editing ? @"done_btn.png" : @"edit_btn.png";
	NSString * title = editing ? NSLocalizedString(@"Done", @"Done") : NSLocalizedString(@"Edit", @"Edit");
	[button setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
	[button setTitle:title forState:UIControlStateNormal];
	button.titleLabel.font = [UIFont boldSystemFontOfSize:14];
	[button addTarget:self action:@selector(toggleEditing) forControlEvents:UIControlEventTouchUpInside];
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
	
	[dataController setEdit:editing];
}

-(void)backAction{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)showTab:(BOOL)show{
  if(show){
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    for(UIView *view in self.tabBarController.view.subviews)
    {
      if([view isKindOfClass:[UITabBar class]]){
        [view setFrame:CGRectMake(view.frame.origin.x, 480-49, view.frame.size.width, view.frame.size.height)];
      } 
      else {
        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 480-49)];
      }
    }
    [UIView commitAnimations];	
  }else{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    for(UIView *view in self.tabBarController.view.subviews){
      if([view isKindOfClass:[UITabBar class]])
      {
        [view setFrame:CGRectMake(view.frame.origin.x, 480, view.frame.size.width, view.frame.size.height)];
      } 
      else 
      {
        [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 480)];
      }
    }
    [UIView commitAnimations];
  }
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	//	back
	UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
	button.bounds = CGRectMake(0, 0, 65.0, 30.0);
    [button setBackgroundImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
	[button setTitle:NSLocalizedString(@"Back", @"Back") forState:UIControlStateNormal];
   	button.titleLabel.font = [UIFont boldSystemFontOfSize:14];
	button.titleEdgeInsets = UIEdgeInsetsMake(0, 12, 0, 0);
	[button addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
	
    //	edit
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.bounds = CGRectMake(0, 0, 65.0, 30.0);
    [button setBackgroundImage:[UIImage imageNamed:@"edit_btn.png"] forState:UIControlStateNormal];
	[button setTitle:NSLocalizedString(@"Edit", @"Edit") forState:UIControlStateNormal];
   	button.titleLabel.font = [UIFont boldSystemFontOfSize:14];
	[button addTarget:self action:@selector(toggleEditing) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    editing = NO;
	

	termToShow = [[NSMutableArray alloc] init];
	[self loadKeys];
	[self.view bringSubviewToFront:autoCompleteTV];	
	
	keyL.text = NSLocalizedString(@"Test", @"Test");
	valueL.text = NSLocalizedString(@"Value", @"Value");
	[addBtn setTitle:NSLocalizedString(@"Add", @"Add") forState:UIControlStateNormal];
	[submitBtn setTitle:NSLocalizedString(@"Start Analysis", @"Start Analysis") forState:UIControlStateNormal];
	
//	NSError * error;
//	if (![[GANTracker sharedTracker] trackPageview:@"/inputview"
//										 withError:&error]) {
//	}

}

-(void)viewDidAppear:(BOOL)animated{
	termToShow = [[NSMutableArray alloc] init];
	[self loadKeys];
	[self.view bringSubviewToFront:autoCompleteTV];	
}

- (BOOL)loadKeys{
	AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
	//	entrys
	if (delegate.allEntrys != nil) {
		delegate.allEntrys = nil;
	}
	delegate.allEntrys = [[NSMutableArray alloc] initWithArray:[delegate.everyEntry objectAtIndex:delegate.userCategory]];
    
	//	SearchTerms
	if (delegate.allSearchTerms != nil) {
		delegate.allSearchTerms = nil;
	}
	delegate.allSearchTerms = [[NSMutableArray alloc] init];
	
	SearchTerm * tempTerm = nil;
	for (NSInteger index = 0; index < [delegate.allEntrys count]; index++) {
		Entry * entry = (Entry*)[delegate.allEntrys objectAtIndex:index];
		if(tempTerm == nil){
			//	first
			tempTerm = [[SearchTerm alloc] initWithArray:[NSArray arrayWithObjects:entry.formal_name,entry.chinese_name,entry.eng_sym,entry.cht_sym,nil]];
		}
		else if([tempTerm.formal_name isEqualToString:entry.formal_name]){
			//	repeat
		}
		else{
			//	new item
			[delegate.allSearchTerms addObject:tempTerm];
			tempTerm = [[SearchTerm alloc] initWithArray:[NSArray arrayWithObjects:entry.formal_name,entry.chinese_name,entry.eng_sym,entry.cht_sym,nil]];
		}
		[tempTerm addIndex:index];
	}
	//	last
	[delegate.allSearchTerms addObject:tempTerm];
	return YES;
}


/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations.
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

//	textfield delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
	autoView.hidden = NO;
	NSString *substring = keyTF.text;
	if(![string isEqualToString:@"\n"])substring = [substring stringByReplacingCharactersInRange:range withString:string];
	[self searchAutocompleteEntriesWithSubstring:substring];
	return YES;	
}

- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring {
	[termToShow removeAllObjects];
	AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
	NSAssert(delegate.allSearchTerms != nil,@"allSearchTerms = null");
	for (SearchTerm * term in delegate.allSearchTerms) {
		if ([term match:substring]) {
			[termToShow addObject:term];
		}
	}
	[autoCompleteTV reloadData];
}

//	tableview
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [termToShow count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
	SearchTerm * term = (SearchTerm*)[termToShow objectAtIndex:indexPath.row];
	NSArray * eng_cht = [term.stringToShow componentsSeparatedByString:@"、"];
    cell.textLabel.text = [eng_cht objectAtIndex:1];
	cell.detailTextLabel.text = [eng_cht objectAtIndex:0];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[self selectKey:indexPath.row];
}

//	more action
- (IBAction) autocomplete{
	if ([termToShow count] == 0) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No this key" message:@"please input a correct key" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
		[self keyDone:nil];
		return;
	}
	[self selectKey:0];
}

-(void) selectKey:(NSInteger)row{
	SearchTerm * term = (SearchTerm*)[termToShow objectAtIndex:row];
	NSArray * eng_cht = [term.stringToShow componentsSeparatedByString:@"、"];
	if ([keyTF.text characterAtIndex:0] == [[eng_cht objectAtIndex:0] characterAtIndex:0]) {
		keyTF.text = [eng_cht objectAtIndex:0];
	}
	else {
		keyTF.text = [eng_cht objectAtIndex:1];
	}
	if ([term.entryIndexes count] == 1) {
		[self keyDone:term];
	}
	else{
		NSMutableArray * source = [NSMutableArray arrayWithObject:@"檢體種類"];
		NSMutableArray * age = [NSMutableArray arrayWithObject:@"年齡"];
		NSMutableArray * time = [NSMutableArray arrayWithObject:@"時間"];
		NSMutableArray * pose = [NSMutableArray arrayWithObject:@"用什麼姿勢抽血"];
		NSMutableArray * mc = [NSMutableArray arrayWithObject:@"月經狀況"];
		NSMutableArray * pregnant = [NSMutableArray arrayWithObject:@"懷孕"];
		NSMutableArray * food = [NSMutableArray arrayWithObject:@"飲食"];
		NSMutableArray * medicine = [NSMutableArray arrayWithObject:@"使用抗凝血藥物"];
		
		for(NSNumber * number in term.entryIndexes){
			AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
			Entry * e = [delegate.allEntrys objectAtIndex:[number intValue]];
//			if([e.source length] != 0){
				[source addObject:e.source];
//			}
//			if([e.age length] != 0){
				[age addObject:e.age];
//			}
//			if([e.time length] != 0){
				[time addObject:e.time];
//			}
//			if([e.pose length] != 0){
				[pose addObject:e.pose];
//			}
//			if([e.mc length] != 0){
				[mc addObject:e.mc];
//			}
//			if([e.pregnant length] != 0){
				[pregnant addObject:e.pregnant];
//			}
//			if([e.food length] != 0){
				[food addObject:e.food];
//			}
//			if([e.medicine length] != 0){
				[medicine addObject:e.medicine];
//			}
		}
		NSMutableArray * dataArray = [[NSMutableArray alloc] initWithObjects:source,age,time,pose,mc,pregnant,food,medicine,nil];
		NSMutableArray * tmpArray = [[NSMutableArray alloc] initWithArray:dataArray];
		for (NSMutableArray * cat in dataArray) {
			BOOL empty = YES;
			for(int i = 1 ; i < [cat count] ; i ++){
				if([[cat objectAtIndex:i] length] != 0){
					empty = NO;
					break;
				}
			}
			if(empty){
//			if ([cat count] == 1) {
				[tmpArray removeObject:cat];
				continue;
			}
			NSString * prev = [cat objectAtIndex:1];
			int i;
			for (i = 2; i < [cat count]; i++) {
				if (![prev isEqualToString:[cat objectAtIndex:i]]) {
					break;
				}
			}
			if (i == [cat count]) {
				[tmpArray removeObject:cat];
			}
		}
		NSLog(@"%@",tmpArray);
		[self detailInfo:[NSArray arrayWithArray:tmpArray] index:term.entryIndexes];
	}
}

-(IBAction) inputValueFinish:(id)sender{
	if (nowEntry == nil) {
		UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"請輸入檢測項目" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
		[alert show];
		return;
	}
	//	value
	NSString * value = valueTF.text;
	nowEntry.value = value;
	AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
	[delegate.inputedData addObject:nowEntry];
	nowEntry = nil;
    
	[dataController.tableView reloadData];
	
	keyTF.text = @"";
	valueTF.text = @"";
	valueTF.enabled = NO;
	valueTF.background = [UIImage imageNamed:@"inactive_field"];
	[keyTF resignFirstResponder];
}

-(IBAction) submit{
	AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
	NSMutableArray * indexes = [[NSMutableArray alloc] init];
	for (Entry * entry in delegate.inputedData) {
		[indexes addObject:[NSArray arrayWithObjects:	[NSNumber numberWithInt:delegate.userCategory],
                            [NSNumber numberWithInt:[delegate.allEntrys indexOfObject:entry]],
                            entry.value,
                            nil]];
	}
	DBHelper *dbHelper = [DBHelper newInstance];
	[dbHelper addHistory:indexes];
	
	DetailTableViewController * dtvc = [[DetailTableViewController alloc] init];
	[self.navigationController pushViewController:dtvc animated:YES];
}

//	helper func
-(void)keyDone:(SearchTerm*)term{
	autoView.hidden = YES;
	valueTF.enabled = YES;
	valueTF.background = [UIImage imageNamed:@"field.png"];
	
	AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
	nowEntry = (Entry*)[delegate.allEntrys objectAtIndex:[[term.entryIndexes objectAtIndex:0] intValue]];
  [valueTF becomeFirstResponder];
}

- (void)DatePickerDoneClick{
  NSLog(@"Done : %d",picker.entryIndex);
  if (picker.entryIndex == -1) {
    return;
  }
  [aac dismissWithClickedButtonIndex:0 animated:YES];
  
  autoView.hidden = YES;
	valueTF.enabled = YES;
	valueTF.background = [UIImage imageNamed:@"field.png"];
  
  AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
	nowEntry = (Entry*)[delegate.allEntrys objectAtIndex:picker.entryIndex];
	keyTF.text = [NSString stringWithFormat:@"%@%@",keyTF.text,picker.output_postfix_];
  [valueTF becomeFirstResponder];
}

- (void)detailInfo:(NSArray *) cat_data index:(NSArray*)indexes{
	aac = [[UIActionSheet alloc] initWithTitle:@"How many?"
													 delegate:self
											cancelButtonTitle:nil
									   destructiveButtonTitle:nil
											otherButtonTitles:nil];
	picker = [[MyDetailPicker alloc] initWithCat:cat_data index:indexes];
	picker.frame = CGRectMake(0, 44.0, picker.frame.size.width, picker.frame.size.height);
    
	UIToolbar * pickerDateToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
	pickerDateToolbar.barStyle = UIBarStyleBlackOpaque;
	[pickerDateToolbar sizeToFit];
	
	NSMutableArray *barItems = [[NSMutableArray alloc] init];
	
	UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
	[barItems addObject:flexSpace];
	
	UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(DatePickerDoneClick)];
	[barItems addObject:doneBtn];
	
	[pickerDateToolbar setItems:barItems animated:YES];
	
	[aac addSubview:pickerDateToolbar];
	[aac addSubview:picker];
	[aac showInView:self.view];
	[aac setBounds:CGRectMake(0,0,320, 464)];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
	valueTF.text = [actionSheet buttonTitleAtIndex:buttonIndex];
	valueTF.enabled = NO;
//	NSString * text = [actionSheet buttonTitleAtIndex:buttonIndex];
//	if ([Utils is:text contain:@"+"]) {
//		valueTF.text = @"+";
//	}
//	else if ([Utils is:text contain:@"-"]) {
//		valueTF.text = @"-";
//	}
}

@end
