//
//  BasicHistory.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 12/12/15.
//
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "TypeDefininition.h"

@interface BasicHistory : NSObject

@property (nonatomic) CGFloat tall_;
@property (nonatomic) CGFloat weight_;
@property (nonatomic) CGFloat waist_;
@property (nonatomic) CGFloat fat_;
@property (nonatomic) CGFloat high_blood_;
@property (nonatomic) CGFloat low_blood_;

-(id)initWithSqliteStatement:(sqlite3_stmt*)stmt;
-(NSArray*)SplitToTypes;
+(NSString*)typeToName:(enum BasicHistoryType)type;
+(NSString*)typeToDBColumnName:(enum BasicHistoryType)type;

@end
