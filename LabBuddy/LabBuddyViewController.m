//
//  LabBuddyViewController.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/10/12.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "LabBuddyViewController.h"
#import "InputViewController.h"
//#import "GANTracker.h"
#import "BasicDataInputViewController.h"

@implementation LabBuddyViewController

/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	maleL.text = NSLocalizedString(@"Male", @"Male");
	femaleL.text = NSLocalizedString(@"Female", @"Female");
	boyL.text = NSLocalizedString(@"Boy", @"Boy");
	boySubL.text = NSLocalizedString(@"Age 0-17", @"Age 0-17");
	girlL.text = NSLocalizedString(@"Girl", @"Girl");
	girlSubL.text = NSLocalizedString(@"Age 0-17", @"Age 0-17");
	chooseL.text = NSLocalizedString(@"Select", @"Select");
}



/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (void)viewDidAppear:(BOOL)animated{
	
	
	AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
	if(delegate.inputedData == nil)
		delegate.inputedData = [[NSMutableArray alloc] init];
	else {
		[delegate.inputedData removeAllObjects];
	}
}

//	action
- (IBAction)selectCategory:(id)sender{
	UIButton * btn = (UIButton*)sender;
	
	AppDelegate * delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
	delegate.userCategory = btn.tag;

	BasicDataInputViewController * controller = [[BasicDataInputViewController alloc] init];
	[self.navigationController pushViewController:controller animated:YES];

//	NSError * error;
//	if (![[GANTracker sharedTracker] trackEvent:@"user"
//										 action:@"click"
//										  label:[NSString stringWithFormat:@"%d",btn.tag]
//										  value:1
//									  withError:&error]) {
//		
//	}	
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
	if(datePicker == nil){
		return;
	}
	NSDate * date = datePicker.date;
	AppDelegate * delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
	delegate.targetDate = date;
	NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"yyyy.MM.dd"];
	NSString * dateStr = [formatter stringFromDate:date];
	[dateBtn setTitle:dateStr forState:UIControlStateNormal];
}

- (IBAction)selectDate{
	NSString *title = UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation) ? @"\n\n\n\n\n\n\n\n\n" : @"\n\n\n\n\n\n\n\n\n\n\n\n" ;
	UIActionSheet *actionSheet = [[UIActionSheet alloc] 
								  initWithTitle:[NSString stringWithFormat:@"%@%@", title, NSLocalizedString(@"SelectADateKey", @"")]
								  delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"Ok", nil];
	[actionSheet showInView:self.view];
	datePicker = [[UIDatePicker alloc] init];
	datePicker.datePickerMode = UIDatePickerModeDate;
	[actionSheet addSubview:datePicker];
}

@end
