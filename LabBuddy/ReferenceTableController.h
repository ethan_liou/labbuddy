//
//  ReferenceTableController.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 12/2/1.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReferenceTableController : UITableViewController{
  NSArray * refs_;
  
}

@property(nonatomic, strong) NSArray * refs_;

@end