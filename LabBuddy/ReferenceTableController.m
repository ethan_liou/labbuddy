//
//  ReferenceTableController.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 12/2/1.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ReferenceTableController.h"
#import "AppDelegate.h"
#import "Entry.h"
//#import "GANTracker.h"


@implementation ReferenceTableController
@synthesize refs_;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [refs_ count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ReferenceCell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
	cell.textLabel.font = [UIFont systemFontOfSize:16];
    cell.textLabel.text = [refs_ objectAtIndex:indexPath.row];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
  AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
  Entry * e = nil;
  if(delegate.allEntrys != nil){
    for(Entry * entry in delegate.allEntrys){
      if([entry.chinese_name isEqualToString:[refs_ objectAtIndex:indexPath.row]]){
        e = entry;
        e.category = delegate.userCategory;
        e.index = [delegate.allEntrys indexOfObject:entry];
        break;
      }
    }
  }
  if(e == nil){
    for(NSArray * entryArr in delegate.everyEntry){
      if([delegate.everyEntry indexOfObject:entryArr] == 0)continue;
      for(Entry * entry in entryArr){
        if([entry.chinese_name isEqualToString:[refs_ objectAtIndex:indexPath.row]]){
          e = entry;
          e.category = [delegate.everyEntry indexOfObject:entryArr];
          e.index = [entryArr indexOfObject:entry];
          break;
        }
      }
      if(e != nil)break;
    }
  }
	if(e == nil){
//		NSError * error;
//		if (![[GANTracker sharedTracker] trackEvent:@"entry"
//											 action:@"no_data"
//											  label:[refs_ objectAtIndex:indexPath.row]
//											  value:1
//										  withError:&error]) {
//		}

		UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"查無資料" message:@"資料補完中" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
		[alert show];
	}
	else
		[[NSNotificationCenter defaultCenter] postNotificationName:@"switchBack" object:e];
}

@end
