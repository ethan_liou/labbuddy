//
//  NewHistoryViewController.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 13/1/20.
//
//

#import "NewHistoryViewController.h"
#import "BasicHistory.h"
#import "Entry.h"
#import "DataListViewController.h"

@interface NewHistoryViewController (){
	IBOutlet UIImageView * header_iv_;
	IBOutlet UITableView * content_tv_;
	IBOutlet UILabel * header_l_;
	NSArray * data_array_;
	enum DataType data_type_;
}

@end

@implementation NewHistoryViewController

-(id)initWithType:(enum DataType)data_type dataArray:(NSArray*)data_array{
	{
		self = [super initWithNibName:@"NewHistoryViewController" bundle:nil];
		if (self) {
			data_type_ = data_type;
			if(data_type == DATA_BASIC_TYPE){
				NSMutableSet * set = [[NSMutableSet alloc] init];
				for (BasicHistory * bh in data_array) {
					NSArray * types = [bh SplitToTypes];
					for (NSNumber * type in types) {
						[set addObject:type];
					}
				}
				data_array_ = [NSArray arrayWithArray:[set allObjects]];
			}
			else{
				data_array_ = data_array;
			}
		}
		return self;
	}
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	if(data_type_ == DATA_BASIC_TYPE){
		[header_iv_ setImage:[UIImage imageNamed:@"main_data_header.png"]];
		[header_l_ setText:NSLocalizedString(@"Basic Information", @"Basic Information")];
	}
	else{
		[header_iv_ setImage:[UIImage imageNamed:@"inspect_header.png"]];
		[header_l_ setText:NSLocalizedString(@"Test", @"Test")];
	}
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [data_array_ count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	static NSString *CellIdentifier = @"NewHistoryCell";
	
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
	cell.selectionStyle = UITableViewCellSelectionStyleGray;
	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
	cell.textLabel.textColor = [UIColor colorWithRed:77.0/255 green:78.0/255 blue:110.0/255 alpha:1.0];
	if (data_type_ == DATA_BASIC_TYPE) {
		cell.textLabel.text = [BasicHistory typeToName:((NSNumber*)[data_array_ objectAtIndex:indexPath.row]).intValue];
	}
	else{
		Entry * e = [data_array_ objectAtIndex:indexPath.row];
		cell.textLabel.text = e.chinese_name;
	}
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	[content_tv_ deselectRowAtIndexPath:indexPath animated:YES];
	DataListViewController * dlvc = [[DataListViewController alloc] initWithType:data_type_ Object:[data_array_ objectAtIndex:indexPath.row]];
	[self.navigationController pushViewController:dlvc animated:YES];
}

@end
