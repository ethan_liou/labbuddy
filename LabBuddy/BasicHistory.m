//
//  BasicHistory.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 12/12/15.
//
//

#import "BasicHistory.h"

@implementation BasicHistory

@synthesize fat_,high_blood_,low_blood_,tall_,waist_,weight_;

-(id)initWithSqliteStatement:(sqlite3_stmt*)stmt{
	self = [super init];
	if(self){
		self.tall_ = sqlite3_column_double(stmt, 2);
		self.weight_ = sqlite3_column_double(stmt, 3);
		self.waist_ = sqlite3_column_double(stmt, 4);
		self.fat_ = sqlite3_column_double(stmt, 5);
		self.high_blood_ = sqlite3_column_double(stmt, 6);
		self.low_blood_ = sqlite3_column_double(stmt, 7);
	}
	return self;
}

-(NSArray*)SplitToTypes{
	NSMutableArray * names = [[NSMutableArray alloc] init];
	if (tall_ != 0.0f) {
		[names addObject:[NSNumber numberWithInt:HEIGHT]];
	}
	if (weight_ != 0.0f) {
		[names addObject:[NSNumber numberWithInt:WEIGHT]];
	}
	if (waist_ != 0.0f) {
		[names addObject:[NSNumber numberWithInt:WAIST]];
	}
	if (fat_ != 0.0f) {
		[names addObject:[NSNumber numberWithInt:FAT]];
	}
	if (high_blood_ != 0.0f) {
		[names addObject:[NSNumber numberWithInt:BP_H]];
	}
	if (low_blood_ != 0.0f) {
		[names addObject:[NSNumber numberWithInt:BP_L]];
	}
	return names;
}

+(NSString*)typeToName:(enum BasicHistoryType)type{
	switch (type) {
		case HEIGHT:
			return NSLocalizedString(@"Height", @"Height");
		case WEIGHT:
			return NSLocalizedString(@"Weight", @"Weight");
		case WAIST:
			return NSLocalizedString(@"Waist", @"Waist");
		case FAT:
			return NSLocalizedString(@"Body Fat Percentage", @"Body Fat Percentage");
		case BP_H:
			return NSLocalizedString(@"Systolic Pressure", @"Systolic Pressure");
		case BP_L:
			return NSLocalizedString(@"Diastolic Pressure", @"Diastolic Pressure");
		default:
			return @"";
	}
	return @"";
}

+(NSString*)typeToDBColumnName:(enum BasicHistoryType)type{
	switch (type) {
		case HEIGHT:
			return @"height";
		case WEIGHT:
			return @"weight";
		case WAIST:
			return @"waist";
		case FAT:
			return @"fat";
		case BP_H:
			return @"bp_h";
		case BP_L:
			return @"bp_l";
		default:
			return @"";
	}
	return @"";
}

@end
