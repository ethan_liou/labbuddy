//
//  CalendarViewControllerViewController.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 12/7/4.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalendarViewControllerViewController : UIViewController{
	IBOutlet UIView * day_v_;
	IBOutlet UILabel * date_l_;
	IBOutlet UIButton * basic_b_;
	IBOutlet UIButton * history_b_;
	NSInteger year_;
	NSInteger month_;
	NSInteger day_;
	NSInteger now_year_;
	NSInteger now_month_;
	NSInteger now_day_;
}

-(IBAction)detailData:(id)sender;
-(IBAction)nextMonth;
-(IBAction)prevMonth;

@end
