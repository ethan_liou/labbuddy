//
//  DataTableViewController.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/10/14.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DataTableViewController.h"
#import "AppDelegate.h"
#import "ItemCell.h"
//#import "GANTracker.h"

@implementation DataTableViewController

#pragma mark -
#pragma mark View lifecycle


//-(id)initWithCoder:(NSCoder *)aDecoder{
//    self = [super init];
//    if (self) {
//		data = [[NSMutableDictionary alloc] init];
//    }
//    return self;			
//}
//
//	add or remove data
//- (BOOL) addDataWithKey:(NSString *)key Value:(NSString *)value{
//	[data setObject:value forKey:key];
//	NSLog(@"Add %@ = %@",key,value);
//	
//	[self.tableView reloadData];
//	return YES;
//}
//
//- (BOOL) removeDataWithKey:(NSString *)key{
//	[data removeObjectForKey:key];
//	NSLog(@"Remove %@",key);
//	[self.tableView reloadData];
//	return YES;
//}

/*
- (void)viewDidLoad {
    [super viewDidLoad];

    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
	//	controller.navigationItem.rightBarButtonItem = self.editButtonItem;
}
*/
/*
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
*/
/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}
*/
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
	AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
	NSLog(@"Count %d",[delegate.inputedData count]);
    return [delegate.inputedData count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"ItemCell";
	
	cell = (ItemCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil) {
        [[NSBundle mainBundle] loadNibNamed:@"ItemCell" owner:self options:nil];
    }
	

	AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
	[cell setDataWithEntry:[delegate.inputedData objectAtIndex:indexPath.row]];
	[cell setEdit:isEdit];
	[cell setId:indexPath.row];
//
//	NSError * error;
//	if (![[GANTracker sharedTracker] trackEvent:@"entry"
//										 action:@"input"
//										  label:[NSString stringWithFormat:@"%d",delegate.userCategory]
//										  value:1
//									  withError:&error]) {
//		
//	}	
	
	return cell;
}

-(IBAction) deleteCell:(id)sender{
	UIButton * btn = (UIButton*)sender;
	NSInteger rowId = btn.tag - 1000;
	AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
	[delegate.inputedData removeObjectAtIndex:rowId];
	[self.tableView reloadData];
//	NSError * error;
//	if (![[GANTracker sharedTracker] trackEvent:@"entry"
//										 action:@"delete"
//										  label:[NSString stringWithFormat:@"%d",delegate.userCategory]
//										  value:1
//									  withError:&error]) {
//		
//	}		
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

-(void)setEdit:(BOOL)edit{
	isEdit = edit;
	[self.tableView reloadData];
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source.
		AppDelegate * delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
		[delegate.inputedData removeObjectAtIndex:indexPath.row];
		[self.tableView reloadData];
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
    /*
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
    // ...
    // Pass the selected object to the new view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
    [detailViewController release];
    */
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Relinquish ownership any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    // For example: self.myOutlet = nil;
}

@end

