//
//  MenuObj.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 12/5/28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "MenuObj.h"

@implementation MenuObj

+(id)MenuWithTitle:(NSString*)title Img:(NSString*)img_name Controller:(UIViewController*)ctl{
	return [[MenuObj alloc] initWithTitle:title Img:img_name Controller:ctl];
}

-(id)initWithTitle:(NSString*)title Img:(NSString*)img_name Controller:(UIViewController*)ctl{
	self = [super init];
	if(self){
		title_ = title;
		img_name_ = img_name;
		ctl_ = ctl;
	}
	return self;
}

-(NSString*)title{return title_;}
-(NSString*)img_name{return img_name_;}
-(UIViewController*)ctl{return ctl_;}

@end
