//
//  MenuTableViewController.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 12/5/27.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "MenuTableViewController.h"
#import "MenuTableViewCell.h"
#import "MenuObj.h"
#import "LabBuddyViewController.h"
#import "MyHistoryViewController.h"
#import "BookmarkViewController.h"
#import "ZUUIRevealController.h"
#import "MyNavController.h"
#import "AboutUsViewController.h"
#import "CalendarViewControllerViewController.h"
#import "DataListViewController.h"

@interface MenuTableViewController ()

@end

@implementation MenuTableViewController

static NSString *CellId = @"MenuTableViewCell";
static UINib *cell_loader;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if(self){
		cell_loader = [UINib nibWithNibName:CellId bundle:[NSBundle mainBundle]];
		
		//	basic
		LabBuddyViewController * lbvc = [[LabBuddyViewController alloc] initWithNibName:@"LabBuddyViewController" bundle:nil];
		MenuObj * search_obj = [MenuObj MenuWithTitle:NSLocalizedString(@"Search", @"Search") Img:@"search_icon.png" Controller:lbvc];
		
//		MyHistoryViewController * mhvc = [[MyHistoryViewController alloc] initWithNibName:@"MyHistoryViewController" bundle:nil];
//		MenuObj * history_obj = [MenuObj MenuWithTitle:NSLocalizedString(@"My record", @"My record") Img:@"record_icon.png" Controller:mhvc];
		
		CalendarViewControllerViewController * cvcvc = [[CalendarViewControllerViewController alloc] initWithNibName:@"CalendarViewControllerViewController" bundle:nil];
		MenuObj * calendar_obj = [MenuObj MenuWithTitle:NSLocalizedString(@"My record", @"My record") Img:@"record_icon.png" Controller:cvcvc];
		
		BookmarkViewController * bvc = [[BookmarkViewController alloc] initWithNibName:@"BookmarkViewController" bundle:nil];
		MenuObj * bookmark_obj = [MenuObj MenuWithTitle:NSLocalizedString(@"Bookmark", @"Bookmark") Img:@"bookmark_icon.png" Controller:bvc];
				
		MenuObj * news_obj = [MenuObj MenuWithTitle:NSLocalizedString(@"News", @"News") Img:@"news_icon.png" Controller:nil];
		NSArray * basic_arr = [NSArray arrayWithObjects:search_obj,bookmark_obj,calendar_obj,news_obj,nil];
		
		//	social
		MenuObj *blog_obj = [MenuObj MenuWithTitle:NSLocalizedString(@"WeCareU Blog", @"WeCareU Blog") Img:@"blog_icon.png" Controller:nil];
		MenuObj *fb_obj = [MenuObj MenuWithTitle:NSLocalizedString(@"Facebook", @"Facebook") Img:@"facebook_icon.png" Controller:nil];
		MenuObj *google_obj = [MenuObj MenuWithTitle:NSLocalizedString(@"Google+", @"Google+") Img:@"google_icon.png" Controller:nil];
		MenuObj *twitter_obj = [MenuObj MenuWithTitle:NSLocalizedString(@"Twitter", @"Twitter") Img:@"twitter_icon.png" Controller:nil];
		NSArray * social_arr = [NSArray arrayWithObjects:blog_obj,fb_obj,google_obj,twitter_obj,nil];
		
		//	contacts
		MenuObj * email_obj = [MenuObj MenuWithTitle:NSLocalizedString(@"Contact us", @"Contact us") Img:@"mail_icon.png" Controller:nil];

		AboutUsViewController * auvc = [[AboutUsViewController alloc] initWithNibName:@"AboutUsViewController" bundle:nil];
		MenuObj * about_obj = [MenuObj MenuWithTitle:NSLocalizedString(@"About us", @"About us") Img:@"aboutus_icon.png" Controller:auvc];		
		NSArray * contacts_arr = [NSArray arrayWithObjects:email_obj,about_obj,nil];
		
		//	tools
		MenuObj * doctor_obj = [MenuObj MenuWithTitle:NSLocalizedString(@"Find doctor", @"Find doctor") Img:@"dr_icon.png" Controller:nil];
		MenuObj * hospital_obj = [MenuObj MenuWithTitle:NSLocalizedString(@"Find hospital", @"Find hospital") Img:@"hospital_icon.png" Controller:nil];
		NSArray * tools_arr = [NSArray arrayWithObjects:doctor_obj,hospital_obj,nil];
		
		//	settings
		MenuObj * setting_obj = [MenuObj MenuWithTitle:NSLocalizedString(@"Settings", @"Settings") Img:@"setting_icon.png" Controller:nil];
		NSArray * settings_arr = [NSArray arrayWithObjects:setting_obj, nil];
		
		content_arr_ = [NSMutableArray arrayWithObjects:basic_arr,social_arr,contacts_arr,tools_arr,settings_arr, nil];
	}
	return self;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"slide_bg.png"]];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
	UIView * v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 32)];
	UIImageView * iv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 32)];
	iv.image = [UIImage imageNamed:@"slide_bar.png"];
	UILabel * lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 250, 32)];
	lbl.backgroundColor = [UIColor clearColor];
	lbl.textColor = [UIColor whiteColor];
	lbl.textAlignment = UITextAlignmentCenter;
	NSString * title = nil;
	switch (section) {
		case 0:
			title = @"Lab Buddy";
			break;
		case 1:
			title =	NSLocalizedString(@"Social", @"Social");
			break;
		case 2:
			title = NSLocalizedString(@"Contact", @"Contact");
			break;
		case 3:
			title = NSLocalizedString(@"Tools", @"Tools");
			break;
		case 4:
			title = NSLocalizedString(@"General", @"General");
			break;
		default:
			break;
	}
	lbl.text = title;
	[v addSubview:iv];
	[v addSubview:lbl];
	return v;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [content_arr_ count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[content_arr_ objectAtIndex:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	MenuTableViewCell * cell = (MenuTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellId];
	if(cell == nil){
		NSArray * top_lvl_items = [cell_loader instantiateWithOwner:self options:nil];
		cell = [top_lvl_items objectAtIndex:0];
	}
	MenuObj * obj = [[content_arr_ objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
	cell.img_.image = [UIImage imageNamed:[obj img_name]];
	cell.lbl_.text = [obj title];
	return cell;	
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	MenuObj * obj = [[content_arr_ objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
	if ([obj ctl] != nil) {
		ZUUIRevealController *revealController = [self.parentViewController isKindOfClass:[ZUUIRevealController class]] ? (ZUUIRevealController *)self.parentViewController : nil;
		//	same
		UINavigationController * nav = (UINavigationController*)revealController.frontViewController;
		if ([[obj ctl] isKindOfClass:nav.topViewController.class]) {
			NSLog(@"Same");
			[revealController revealToggle:self];
			return;
		}
		MyNavController * nav_controller = [[MyNavController alloc] initWithRootViewController:[obj ctl]];
		
		[revealController setFrontViewController:nav_controller animated:NO];
	}
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

@end
