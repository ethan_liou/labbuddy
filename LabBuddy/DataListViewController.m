//
//  DataListViewController.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 12/11/24.
//
//

#import "DataListViewController.h"
#import "DataGraphViewController.h"
#import "Entry.h"
#import "DBHelper.h"
#import "BasicHistory.h"
#import "HistoryCell.h"

@interface DataListViewController (){
	IBOutlet UIImageView * bg_iv_;
	IBOutlet UIButton * tag_b_;
	IBOutlet UILabel * name_l_;
	IBOutlet UILabel * date_l_;
	IBOutlet UILabel * value_l_;
	IBOutlet UITableView * content_tv_;
	enum DataType type_;
	NSObject * obj_;
	NSArray * date_val_pairs_;
}

@end

@implementation DataListViewController

BOOL isShowingLandscapeView = NO;

-(id)initWithType:(enum DataType)dataType Object:(NSObject*)obj{
	self = [super initWithNibName:@"DataListViewController" bundle:nil];
    if (self) {
		type_ = dataType;
		obj_ = obj;
		if(type_ == DATA_BASIC_TYPE){
			date_val_pairs_ = [[DBHelper newInstance] queryBasicByType:((NSNumber*)obj).intValue];
			NSLog(@"%@",date_val_pairs_);
		}
    }
    return self;
}

- (void)orientationChanged:(NSNotification *)notification
{
    UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
    if (UIDeviceOrientationIsLandscape(deviceOrientation) &&
        !isShowingLandscapeView)
    {
		DataGraphViewController * dgvc = [[DataGraphViewController alloc] initWithNibName:@"DataGraphViewController" bundle:nil];
		[self.navigationController pushViewController:dgvc animated:NO];
        isShowingLandscapeView = YES;
    }
    else if (UIDeviceOrientationIsPortrait(deviceOrientation) &&
             isShowingLandscapeView)
    {
		[self.navigationController popViewControllerAnimated:NO];
        isShowingLandscapeView = NO;
    }
}

- (void)awakeFromNib
{
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	// load ui
	UIColor * textColor = nil;
	if (type_ == DATA_TEST_TYPE) {
		textColor = [UIColor colorWithRed:64.0/255 green:128.0/255 blue:11.0/255 alpha:1.0];
		Entry* entry = (Entry*)obj_;
		[bg_iv_ setImage:[UIImage imageNamed:@"history_bg_green.jpg"]];
		[tag_b_ setBackgroundImage:[UIImage imageNamed:@"history_tag_green.png"] forState:UIControlStateNormal];
		name_l_.text = [NSString stringWithFormat:@"%@\n%@",entry.formal_name,entry.chinese_name];
	}
	else{
		textColor = [UIColor colorWithRed:50.0/255 green:79.0/255 blue:133.0/255 alpha:1.0];
		NSString * name = [BasicHistory typeToName:((NSNumber*)obj_).intValue];
		name_l_.text = name;
		[bg_iv_ setImage:[UIImage imageNamed:@"history_bg_blue.jpg"]];
		[tag_b_ setBackgroundImage:[UIImage imageNamed:@"history_tag_blue.png"] forState:UIControlStateNormal];
	}
	[tag_b_ setTitle:NSLocalizedString(@"Records", @"Records") forState:UIControlStateNormal];
	date_l_.text = NSLocalizedString(@"Date", @"Date");
	value_l_.text = NSLocalizedString(@"Value", @"Value");
	[tag_b_ setTitleColor:textColor forState:UIControlStateNormal];
	date_l_.textColor = textColor;
	value_l_.textColor = textColor;

	isShowingLandscapeView = NO;
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(orientationChanged:)
												 name:UIDeviceOrientationDidChangeNotification
											   object:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return [date_val_pairs_ count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	static NSString *CellIdentifier = @"HistoryCell";
	
	HistoryCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
		NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"HistoryCell" owner:nil options:nil];
		for(id currentObject in topLevelObjects){
			if([currentObject isKindOfClass:[HistoryCell class]]){
				cell = (HistoryCell *)currentObject;
				break;
			}
		}
	}
    
    // Configure the cell...
	NSArray * date_val = [date_val_pairs_ objectAtIndex:indexPath.row];
	NSNumber * val = ((NSNumber*)[date_val objectAtIndex:1]);
	cell.date_l_.text = [date_val objectAtIndex:0];
	cell.value_l_.text = [val stringValue];
    return cell;
}

@end
