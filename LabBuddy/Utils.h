//
//  Utils.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/10/15.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Utils : NSObject {

}

+(NSInteger)getUserId;
+(BOOL)is:(NSString *)stringA startWith:(NSString *)stringB;
+(BOOL)is:(NSString *)stringA contain:(NSString *)stringB;
+(UIButton*)generateBtn:(NSString *)img_name;

@end
