//
//  DailyHistoryViewController.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/12/24.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DailyHistoryTableCell.h"
#import "HistoryTableViewController.h"

@interface DailyHistoryViewController : UIViewController{
	IBOutlet UITextField * dateTF;
	NSString * dateString;
	NSMutableArray * entrys;
	IBOutlet HistoryTableViewController * controller;
  BOOL editing;
}

-(void)toggleEditing;
- (void)backAction;
- (id)initWithDate:(NSString *)date;
- (void)showTab:(BOOL)isShow;


@end
