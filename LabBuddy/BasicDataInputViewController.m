//
//  BasicDataInputViewController.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 12/5/29.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "BasicDataInputViewController.h"
#import "AppDelegate.h"
#import "DBHelper.h"
#import "InputViewController.h"
#import "MyNavController.h"

@interface BasicDataInputViewController (){
	UITextField * activeTextField;
}

@end

@implementation BasicDataInputViewController

-(IBAction)nextStep{
	NSMutableArray * arr = [[NSMutableArray alloc] init];
	for (NSInteger i = 0; i < 11; i++) {
		[arr addObject:@"NULL"];
	}
	BOOL hasData = NO;
	for (NSInteger i = 0; i<[tv_arr_ count]; i++) {
		NSString * str = ((UITextField*)[tv_arr_ objectAtIndex:i]).text;
		if([str length] != 0){
			[arr replaceObjectAtIndex:i withObject:str];
			hasData = YES;
		}
	}
	if (!hasData){
		NSLog(@"NO data");
	}
	else if(![[DBHelper newInstance] addBasicHistory:arr]){
		NSLog(@"add basic history fail");
	}
	InputViewController * ivc = [[InputViewController alloc] initWithNibName:@"InputViewController" bundle:nil];
	[self.navigationController pushViewController:ivc animated:YES];
}

-(IBAction)hideKeyboard{
	for (UITextField * tf in tv_arr_) {
		[tf resignFirstResponder];
	}
}

- (void)keyboardWillShow:(NSNotification*)noti{
	NSDictionary* info = [noti userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
	CGFloat height = kbSize.height - (self.view.frame.size.height - data_sv_.frame.size.height);
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, height, 0.0);
	
    data_sv_.contentInset = contentInsets;
    data_sv_.scrollIndicatorInsets = contentInsets;

    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;

	if (![activeTextField.superview isKindOfClass:[UIScrollView class]]) {
		
	}	
	CGPoint right_and_bottom_pt = activeTextField.frame.origin;
	right_and_bottom_pt.y += activeTextField.frame.size.height;
	right_and_bottom_pt.x += activeTextField.frame.size.width;
	right_and_bottom_pt.y -= data_sv_.contentOffset.y;
	if(![activeTextField isKindOfClass:[UIScrollView class]]){
		right_and_bottom_pt.y += activeTextField.superview.frame.origin.y;
	}

	// begin : right_and_bottom, end : arect.height - 10, change arect.height - right_and_bottom - 10
    if (!CGRectContainsPoint(aRect, right_and_bottom_pt) ) {
		if(![activeTextField isKindOfClass:[UIScrollView class]]){
			CGRect target = activeTextField.frame;
			target.origin.y += activeTextField.superview.frame.origin.y;
			[data_sv_ scrollRectToVisible:target animated:YES];
		}
		else{
			[data_sv_ scrollRectToVisible:activeTextField.frame animated:YES];
		}
    }
}

- (void)keyboardWillHide:(NSNotification*)noti{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    data_sv_.contentInset = contentInsets;
    data_sv_.scrollIndicatorInsets = contentInsets;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)GenerateTVArray:(UIView *)parent{
	for (UIView * sub in parent.subviews) {
		if ([sub.subviews count] > 0) {
			[self GenerateTVArray:sub];
		}
		else if([sub isKindOfClass:[UITextField class]]){
			[tv_arr_ addObject:sub];
		}
	}
}

-(void)backAction{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
	button.bounds = CGRectMake(0, 0, 65.0, 30.0);
    [button setBackgroundImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
	[button setTitle:NSLocalizedString(@"Back", @"Back") forState:UIControlStateNormal];
   	button.titleLabel.font = [UIFont boldSystemFontOfSize:14];
	button.titleEdgeInsets = UIEdgeInsetsMake(0, 12, 0, 0);
	[button addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];

//	AppDelegate * app_delegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
//	if(app_delegate.userCategory == 2 || app_delegate.userCategory == 4){
//		CGFloat oriHeight = body_iv_.frame.size.height;
//		// woman
//		footer_iv_.frame = CGRectMake(footer_iv_.frame.origin.x, 
//									  footer_iv_.frame.origin.y + woman_v_.frame.size.height,
//									  footer_iv_.frame.size.width, 
//									  footer_iv_.frame.size.height);
//		body_iv_.frame = CGRectMake(body_iv_.frame.origin.x, 
//									  body_iv_.frame.origin.y, 
//									  body_iv_.frame.size.width, 
//									  body_iv_.frame.size.height+woman_v_.frame.size.height);
//		
//		woman_v_.frame = CGRectMake(0, oriHeight, woman_v_.frame.size.width, woman_v_.frame.size.height);
//		[data_sv_ addSubview:woman_v_];
//		data_sv_.contentSize = CGSizeMake(data_sv_.frame.size.width, data_sv_.frame.size.height+woman_v_.frame.size.height);
//	}
//	else{
//		data_sv_.contentSize = CGSizeMake(data_sv_.frame.size.width, data_sv_.frame.size.height);		
//	}
	data_sv_.contentSize = CGSizeMake(data_sv_.frame.size.width, data_sv_.frame.size.height);

	tv_arr_ = [[NSMutableArray alloc] init];
	[self GenerateTVArray:data_sv_];
	
    [[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardWillShow:)
												 name:UIKeyboardDidShowNotification object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardWillHide:)
												 name:UIKeyboardWillHideNotification object:nil];

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) textFieldDidBeginEditing:(UITextField *)textField{
	activeTextField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeTextField = nil;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
	NSInteger idx = [tv_arr_ indexOfObject:textField];
	if(idx == [tv_arr_ count] - 1){
		// last
		[self hideKeyboard];
	}
	else{
		UITextField * tf = [tv_arr_ objectAtIndex:idx+1];
		[tf becomeFirstResponder];	
	}
	return YES;
}

@end
