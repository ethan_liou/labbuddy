//
//  DetailViewController.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/10/15.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Entry.h"
#import "ReferenceTableController.h"
#import "RefViewController.h"

@interface DetailViewController : UIViewController {
	IBOutlet UILabel * formalL;
	IBOutlet UILabel * chtL;
	IBOutlet UILabel * funcL;
	IBOutlet UITextField * valueTF;
	IBOutlet UIButton * refB;
	
	IBOutlet UIView * threeV;
	IBOutlet UILabel * three_upperL;
	IBOutlet UILabel * three_lowerL;
	IBOutlet UIButton * three_upperB;
	IBOutlet UIButton * three_normalB;
	IBOutlet UIButton * three_lowerB;
	
	IBOutlet UIView * twoV;
	IBOutlet UILabel * two_upperL;
	IBOutlet UILabel * two_lowerL;
	IBOutlet UIButton * two_upperB;
	IBOutlet UIButton * two_normalB;
	
	IBOutlet UIButton * high_allB;
	IBOutlet UIButton * normal_allB;
	IBOutlet UIButton * low_allB;
	IBOutlet UILabel * unitL;
	IBOutlet UIButton * showDetailB;
	IBOutlet UIButton * detailB;
	
	RefViewController * refController;
	Entry * entry;
	BOOL isBookmark;
	NSArray * bookmarks;
	BOOL isShow;
	NSTimer * showTabTimer;
}

-(IBAction)showDetailInfo:(id)sender;
- (void)showTab:(BOOL)isShow;
-(IBAction) showDetail:(id)sender;
-(IBAction) hideDetail:(id)sender;
-(IBAction)changeTab;
- (void)backAction;
- (void)addBookmark;
- (void)deleteBookmark;
- (void)setBookmarkBtn;
-(id)initWithEntry:(Entry*)e;
- (void)fillData:(NSString*)data In:(UIButton*)btn;
-(void) loadData;
- (void)handleSingleTap:(id)sender;
- (IBAction)switchView;
- (void) switchBack:(NSNotification *) notification;
@property (nonatomic,strong) Entry * entry;

@end
