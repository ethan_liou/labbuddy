//
//  ItemCell.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/12/22.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Entry.h"

@interface ItemCell : UITableViewCell {
	UIImageView * background;
	IBOutlet UILabel * formal_nameL;
	IBOutlet UILabel * cht_nameL;
	IBOutlet UILabel * valueL;
	IBOutlet UIButton * deleteBtn;
}

- (void) setId:(NSInteger)rowId;
- (void) setEdit:(BOOL)edit;
- (void) setDataWithEntry:(Entry *)entry;

@end
