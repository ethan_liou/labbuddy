//
//  InputViewController.h
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/10/12.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataTableViewController.h"
#import "AppDelegate.h"
#import "SearchTerm.h"
#import "Entry.h"
#import "MyDetailPicker.h"

@interface InputViewController : UIViewController <UITextFieldDelegate,
	UITableViewDelegate,
	UITableViewDataSource,
	UIActionSheetDelegate> {
	IBOutlet UITextField * keyTF;
	IBOutlet UITableView * autoCompleteTV;
	IBOutlet UIButton * submitBtn;
	IBOutlet UIButton * addBtn;
	IBOutlet DataTableViewController * dataController;
	
	//	auto complete
	IBOutlet UIView * autoView;
	
	//	input area
	IBOutlet UITextField * valueTF;

	//	keys
	NSMutableArray * termToShow;
	NSString * tblName;
	Entry * nowEntry;
	
	BOOL editing;
  
	MyDetailPicker * picker;
	UIActionSheet *aac;
	BOOL isShow;
	NSTimer * showTabTimer;
	IBOutlet UILabel * keyL;
	IBOutlet UILabel * valueL;
}

- (IBAction)changeTab;
- (IBAction)selectValueTF;
- (void)showTab:(BOOL)show;
-(void)toggleEditing;
-(void)backAction;
-(BOOL)loadKeys;
-(void)searchAutocompleteEntriesWithSubstring:(NSString *)substring;
-(void)keyDone:(SearchTerm *)term;
-(IBAction) autocomplete;
-(IBAction) inputValueFinish:(id)sender;
-(IBAction) submit;
-(void) selectKey:(NSInteger)row;
- (void)detailInfo:(NSArray *) data index:(NSArray*)indexes;
- (void)DatePickerDoneClick;

@end
