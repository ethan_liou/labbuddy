//
//  Entry.m
//  LabBuddy
//
//  Created by Liou Yu-Cheng on 2011/10/14.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Entry.h"
#import "Utils.h"

@implementation Entry
@synthesize category;
@synthesize index;
@synthesize formal_name;
@synthesize chinese_name;
@synthesize source;
@synthesize age;
@synthesize time;
@synthesize pose;
@synthesize mc;
@synthesize pregnant;
@synthesize food;
@synthesize medicine;
@synthesize usage;
@synthesize unit;
@synthesize upper_bond;
@synthesize lower_bond;
@synthesize red_detail;
@synthesize red_simple;
@synthesize green_detail;
@synthesize green_simple;
@synthesize blue_detail;
@synthesize blue_simple;
@synthesize reference;
@synthesize plus;
@synthesize eng_sym;
@synthesize cht_sym;
@synthesize suggestion;
@synthesize value;

-(NSString*)getCategory{
	NSArray * arr = [NSArray arrayWithObjects:@"",@"成年男性",@"成年女性",@"男孩",@"女孩", nil];
	return (self.category >= 1 && self.category <= 4 ) ? [arr objectAtIndex:self.category] : @"";
}

-(id)initWithArray:(NSArray*)array{
	self = [super init];
    if (self) {
      index = -1;
      category = -1;
		if ([array count] == 24) {
			//	male
			formal_name = [array objectAtIndex:0];
			chinese_name = [array objectAtIndex:1];
			source = [array objectAtIndex:2];
			age = [array objectAtIndex:3];
			time = [array objectAtIndex:4];
			pose = [array objectAtIndex:5];
			mc = @"";
			pregnant = @"";
			food = [array objectAtIndex:6];
			medicine = [array objectAtIndex:7];
			usage = [array objectAtIndex:8];
			unit = [array objectAtIndex:9];
			upper_bond = [array objectAtIndex:10];
			lower_bond = [array objectAtIndex:11];
			red_detail = [array objectAtIndex:12];
			red_simple = [array objectAtIndex:13];
			green_detail = [array objectAtIndex:14];
			green_simple = [array objectAtIndex:15];
			blue_detail = [array objectAtIndex:16];
			blue_simple = [array objectAtIndex:17];
			reference = [array objectAtIndex:18];
			plus = [array objectAtIndex:19];
			eng_sym = [array objectAtIndex:20];
			cht_sym = [array objectAtIndex:21];
			suggestion = [array objectAtIndex:22];			
		}
		else if([array count] == 26){
			//	female
			formal_name = [array objectAtIndex:0];
			chinese_name = [array objectAtIndex:1];
			source = [array objectAtIndex:2];
			age = [array objectAtIndex:3];
			time = [array objectAtIndex:4];
			pose = [array objectAtIndex:5];
			mc = [array objectAtIndex:6];
			pregnant = [array objectAtIndex:7];
			food = [array objectAtIndex:8];
			medicine = [array objectAtIndex:9];
			usage = [array objectAtIndex:10];
			unit = [array objectAtIndex:11];
			upper_bond = [array objectAtIndex:12];
			lower_bond = [array objectAtIndex:13];
			red_detail = [array objectAtIndex:14];
			red_simple = [array objectAtIndex:15];
			green_detail = [array objectAtIndex:16];
			green_simple = [array objectAtIndex:17];
			blue_detail = [array objectAtIndex:18];
			blue_simple = [array objectAtIndex:19];
			reference = [array objectAtIndex:20];
			plus = [array objectAtIndex:21];
			eng_sym = [array objectAtIndex:22];
			cht_sym = [array objectAtIndex:23];
			suggestion = [array objectAtIndex:24];			
		}
	}
	return self;
}


-(NSString *)description{
	return [NSString stringWithFormat:@"%@,%@",formal_name,chinese_name];
}



@end
